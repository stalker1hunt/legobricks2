﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Integrable
{
	//Enums.blockType integrableType;
	public Wall parentWall;
	//[Range(0,1)]
	public float position;
	//[Range(0,parentBlock.lenght)]
	public float lenght;
	public float height;
	public Enums.TypeOfIntegrable typeOfInegrable;

	public void SetParentWall(Wall parentWall)
	{
		this.parentWall = parentWall;
	}

	public Integrable(Wall parentWall)
	{
		this.parentWall = parentWall;
	}
	public Integrable()
	{
		this.parentWall = null;
	}
}
