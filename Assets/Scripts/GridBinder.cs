﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using System.Linq;

public class GridBinder:MonoBehaviour {
	//public int xLenght;
	//public int yLenght;
    [SerializeField]
	public static Vector3[,] gridPoints_little;
    public static Vector3[,] gridPoints_big;

    public static float l_Cell = 0.05f;
    public static float b_Cell = 0.05f;

    public static List<Vector3[]> additionalPoints;
	public float cellSize = 0.05f;

	public int xOffset;
	public int yOffset;

	void Awake()
	{
		SetGrid (Enums.xLenght,Enums.yLenght, Enums.cellSize);
	}
	public void SetGrid(int xLenghtValue,int yLenghtValue, float cellSizeValue)
	{
		if(additionalPoints==null)
		additionalPoints = new List<Vector3[]> ();
	//Test of additional points functionality
//		Vector3[] temp = new Vector3[4];
//		temp [0] = new Vector3 (.5f,.5f,.5f);
//		temp [1] = new Vector3 (1.5f,1.5f,1.5f);
//		temp [2] = new Vector3 (2.5f,2.5f,2.5f);
//		temp [3] = new Vector3 (2.5f,2.5f,2.5f);
//		additionalPoints.Add (temp);


		//Enums.xLenght = xLenghtValue;
		//Enums.yLenght = yLenghtValue;
		//cellSize = cellSizeValue;

        gridPoints_little = BuildGrid(500, 500, l_Cell);
        gridPoints_big = BuildGrid(500, 500, b_Cell);
    }

	Vector3[,] BuildGrid(int _x , int _y , float _celS)
	{
        Vector3 [,] gridList = new Vector3[_x, _y];
        int _x1 = (int)(-gridList.GetLength(0) * 0.5f);
        int _y1 = (int)(-gridList.GetLength(1) * 0.5f);
        print(_x1 + " "+ _y1);
        for (int x = 0; x < gridList.GetLength(0); x++)
        {  for (int y = 0; y < gridList.GetLength(1); y++)
            {
                gridList[x, y] = new Vector3((x-_x1) * _celS, (y-_y1) * _celS);
            }
        }
        return gridList;

    }

	public static Vector3 BindToGrid(Vector3 position, Vector3[,] gridList, float _cellS)
	{
		Vector3 result = new Vector3 ();
		result.x = (int) (Mathf.Round((float)position.x / _cellS));
		result.y = (int) (Mathf.Round((float)position.y / _cellS));

		float distanceToGridPoint = Mathf.Abs(position.x - gridList[(int)result.x, (int)result.y].x) +
									Mathf.Abs(position.y - gridList[(int)result.x, (int)result.y].y);

		float additionalPointsMinimum=10f;
		Vector3 additionalPointPosition=new Vector3();
		if (additionalPoints.Count>0) {
			foreach (Vector3[] vectors in additionalPoints) {
				foreach (Vector3 vector in vectors) {
					float distToAddPoint = Mathf.Abs (position.x - vector.x) +	Mathf.Abs (position.y - vector.y);
					if (additionalPointsMinimum > distToAddPoint) {
						additionalPointsMinimum = distToAddPoint;
						additionalPointPosition = vector;
					}
				}
			}
		
			if (distanceToGridPoint < (Mathf.Abs (position.x - additionalPointPosition.x) + Mathf.Abs (position.y - additionalPointPosition.y))) {
				return gridList[(int)result.x, (int)result.y];
			} else
				return new Vector3 (additionalPointPosition.x, additionalPointPosition.y);
		} else
			return gridList[(int)result.x, (int)result.y];
			
	}

    public static Vector3 BindToPos(Vector3 pos )
    {
        Vector3 v3 = new Vector3((float)System.Math.Round(pos.x, 2),//(float)System.Math.Ceiling(pos.x * 20) / 20.0f,
                       (float)System.Math.Round(pos.y, 2),          //(float)System.Math.Ceiling(pos.y * 20) / 20.0f,
            pos.z);
        return v3;
    }

}
