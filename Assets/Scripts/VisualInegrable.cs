﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualInegrable : Shape {
	float colliderSizeOffset =0.1f;

	public Wall wall;
	public Integrable integrable;
	public float zedPosition;
	public float lenghtEditorInput;
	public bool ApplyEditorChanges;
//	Vector3 onStartDraggingCursorPosition;
//	float onStartDraggingTime;
	float lenght;
	public float Lenght {
		get {
			return lenght;
		}
		set {
			if (lenght != value) {
				lenght = value;
				integrable.lenght = value;
				if(integrable.parentWall!=null)
				UpdateIntegrable ();
			}
		}
	}
	public Material[] materials;
	public float position;
	Enums.TypeOfIntegrable typeOfObject;
	Renderer renderer;
	public Enums.TypeOfIntegrable TypeOfObject {
		get {
			return typeOfObject;
		}
		set {
			if (value != typeOfObject) {	
				integrable.typeOfInegrable = value;
				typeOfObject = value;
				MaterialOfIntegrable (value);
			}
		}
	}

	Vector3 centerOfCollider;
	//Mesh meshVB;
	Vector3[] vertices;
	BoxCollider boxCollider;

	void Awake()
	{	
		renderer = transform.GetComponent<Renderer> ();
		wall = new Wall (Vector3.zero, Enums.defaultWallLenght,Enums.defaultWallHeight, Enums.Orientation.horizontal);
		if (integrable == null) {
			
			integrable = new Integrable (wall);
		}
		integrable.lenght = lenghtEditorInput;
		zedPosition = -2f;
	//	if (base.shapeMesh==null)
		{
		base.shapeMesh = new Mesh ();
		base.shapeMesh.name = "Integrable";
		
		}
		//boxCollider = new BoxCollider ();
		//boxCollider = GetComponent<BoxCollider>();
		if (GetComponent<BoxCollider>() == null) {
			base.shapeCollider = (Collider)gameObject.AddComponent<BoxCollider> ();
		}else
			base.shapeCollider = (Collider)gameObject.GetComponent<BoxCollider> ();

		vertices = new Vector3[4];
		boxCollider = base.GetComponent<BoxCollider> ();
		//meshVB = new Mesh ();
		//meshVB.name = "Block";

	}
	void Start()
	{
	//	Lenght = lenghtEditorInput;
	//	SetPositionRelatedToParentWall ();
	//	UpdateIntegrable ();
	}
	void Update()
	{
		if (ApplyEditorChanges) {
			integrable.position = position;
			Lenght = lenghtEditorInput;
			SetPositionRelatedToParentWall ();
			ApplyEditorChanges = false;
		}
	//	UpdateIntegrable ();
	}

	void OnMouseDown()
	{
		Vector3 mousePosition = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0);
		//Vector3 cursorPosition = Camera.main.ScreenToWorldPoint (mousePosition);
		onStartDraggingCursorPosition = mousePosition;
		onStartDraggingTime = Time.time;
	}
	void OnMouseDrag()
	{
		Vector3 mousePosition = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0);
		Vector3 objPosition = Camera.main.ScreenToWorldPoint (mousePosition);

		if (mousePosition != onStartDraggingCursorPosition) {
			transform.position = new Vector3 (objPosition.x, objPosition.y, 0); //objPosition;
		} else {
			if (Time.time - onStartDraggingTime > Enums.timeToShowParametrs) {
				GameObject managerGo = GameObject.Find ("Manager");
				managerGo.GetComponent<Manager>().selectedObject = gameObject;
				return;
			}
		}
//		Vector3 mousePosition = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0);
//		Vector3 objPosition = Camera.main.ScreenToWorldPoint (mousePosition);
//		if(integrable.parentWall.orientation == Enums.Orientation.bottom||integrable.parentWall.orientation == Enums.Orientation.top)
//		{
//		transform.position = new Vector3 (transform.position.x,objPosition.y,zedPosition); //objPosition;
//		SetParametrs();
//		}else
//		{
//		transform.position = new Vector3 (objPosition.x,transform.position.y,zedPosition); //objPosition;
//		SetParametrs();
//		}
	}
	public void OnMouseUp()
	{
		transform.position = GridBinder.BindToGrid(transform.position, GridBinder.gridPoints_little, GridBinder.l_Cell);
		//integrable.position = transform.position;


	}




	void SetIntegrableVertices(Integrable integrableObj)
	{
		

		switch (integrableObj.parentWall.orientation2) {
		case Enums.Orientation2.top:
			{
				vertices [0] = new Vector3 ( - Enums.depth, 0, zedPosition);
				vertices [1] = new Vector3 ( + Enums.depth, 0, zedPosition);
				vertices [2] = new Vector3 ( - Enums.depth, +integrableObj.lenght, zedPosition);
				vertices [3] = new Vector3 ( + Enums.depth, +integrableObj.lenght, zedPosition);

				centerOfCollider = new Vector3 (0,+integrableObj.lenght/2, zedPosition/2);
				boxCollider.center = centerOfCollider;
				boxCollider.size = new Vector3 (Enums.depth*2,integrableObj.lenght-colliderSizeOffset, .5f);

			}
			break;
		case Enums.Orientation2.bottom:
			{
				vertices [0] = new Vector3 ( - Enums.depth, -integrableObj.lenght, zedPosition);
				vertices [1] = new Vector3 ( + Enums.depth, -integrableObj.lenght, zedPosition);
				vertices [2] = new Vector3 ( - Enums.depth, 0, zedPosition);
				vertices [3] = new Vector3 ( + Enums.depth, 0, zedPosition);

				centerOfCollider = new Vector3 (0, - integrableObj.lenght/2, zedPosition/2);
				boxCollider.center = centerOfCollider;
				boxCollider.size = new Vector3 (Enums.depth*2,integrableObj.lenght-colliderSizeOffset, .5f);

			}
			break;
		case Enums.Orientation2.right:
			{
				vertices [0] = new Vector3 ( 0 ,           		   - Enums.depth, zedPosition);//lb
				vertices [1] = new Vector3 ( integrableObj.lenght, - Enums.depth, zedPosition);//rb
				vertices [2] = new Vector3 ( 0 ,                   + Enums.depth, zedPosition);//lt
				vertices [3] = new Vector3 ( integrableObj.lenght, + Enums.depth, zedPosition);//rt

				centerOfCollider = new Vector3 (+integrableObj.lenght/2, 0, zedPosition/2);
				boxCollider.center = centerOfCollider;
				boxCollider.size = new Vector3 (integrableObj.lenght-colliderSizeOffset, Enums.depth*2, .5f);

			}
			break;
		case Enums.Orientation2.left:
			{	
				vertices [0] = new Vector3 (- integrableObj.lenght,  - Enums.depth, zedPosition);//lb
				vertices [1] = new Vector3 (  0,					 - Enums.depth, zedPosition);//rb
				vertices [2] = new Vector3 (- integrableObj.lenght,  + Enums.depth, zedPosition);//lt
				vertices [3] = new Vector3 (  0,               		 + Enums.depth, zedPosition);//rt



				centerOfCollider = new Vector3 (-integrableObj.lenght/2, 0, 0);
				boxCollider.center = centerOfCollider;
				boxCollider.size = new Vector3 (integrableObj.lenght-colliderSizeOffset, Enums.depth*2, 0.5f);
			}
			break;
		}
		//if(vertices!=null)
		//base.shapeMesh.vertices = vertices; 

	}

	public void SetParametrs()
	{
		//integrable.parentWall = parentWall;

		SetIntegrablePosition ();
   		SetPositionRelatedToParentWall ();
		UpdateIntegrable ();
	}
	void SetIntegrablePosition()
	{
		if (integrable.parentWall.orientation2 == Enums.Orientation2.top || integrable.parentWall.orientation2 == Enums.Orientation2.bottom) 
		{
			integrable.position = Mathf.Abs(transform.position.y-integrable.parentWall.position.y);
		} 
		else 
		{
			integrable.position = Mathf.Abs(transform.position.x-integrable.parentWall.position.x);

		}
		//Debug.Log (integrable.position);
	}

	public void SetPositionRelatedToParentWall()
	{
		switch (integrable.parentWall.orientation2) {
		case Enums.Orientation2.bottom:
			{
				transform.position = 
					new Vector3 (
						integrable.parentWall.position.x,
						integrable.parentWall.position.y-integrable.position,
						zedPosition);
			}
			break;
		case Enums.Orientation2.left:
			{
				
				transform.position = 
					new Vector3 (
						integrable.parentWall.position.x-integrable.position,
						integrable.parentWall.position.y,
						zedPosition);
			}
			break;
		case Enums.Orientation2.right:
			{
				transform.position = 
					new Vector3 (
						integrable.parentWall.position.x+integrable.position,
						integrable.parentWall.position.y,
						zedPosition);
			}
			break;
		case Enums.Orientation2.top:
			{
				transform.position = 
					new Vector3 (
						integrable.parentWall.position.x,
						integrable.parentWall.position.y+integrable.position,
						zedPosition);
			}
			break;
		}

	}
	void SetMesh()
	{	
		int[] triangels = {0,2,1,3,1,2};
		Vector2[] uv = {new Vector2 (0,0),new Vector2 (0,1),new Vector2 (1,0),new Vector2 (1,1)};
		base.SetMesh (vertices, uv, triangels);
	}
	public void UpdateIntegrable()
	{

		vertices = new Vector3[4];
		//block = new Block(Vector3.zero, 4f,ori) ;
		SetIntegrableVertices (integrable);
	//	SetParametrs (integrable.parentWall);
		SetMesh ();
	//	Debug.Log ("Updated");
	}
	public void MaterialOfIntegrable(Enums.TypeOfIntegrable typeOfInteg)
	{
		switch (typeOfInteg) {
		case Enums.TypeOfIntegrable.door:
			{
				renderer.sharedMaterial = Manager.manager.GetComponent<Materials> ().materials [1];
				//Debug.Log("Door");
			}
			break;
		case Enums.TypeOfIntegrable.window:
			{
				//renderer.sharedMaterial = materials [1];
				renderer.sharedMaterial = Manager.manager.GetComponent<Materials> ().materials [2];
				//Debug.Log("Window");
			}
			break;
		}
	}
}
