﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Point : MonoBehaviour {
	Vector3[] vertices;
	Mesh mesh;
	public float heigh;
	public float width;
	// Use this for initialization
	void Start () {
		SetMesh ();
	}
	void SetMesh()
	{
		if (mesh == null) {
			mesh = new Mesh ();
			vertices = new Vector3[4];
			vertices [0] = new Vector3 (-width / 2, -heigh / 2, 0);
			vertices [1] = new Vector3 (width / 2, -heigh / 2, 0);
			vertices [2] = new Vector3 (-width / 2, heigh / 2, 0);
			vertices [3] = new Vector3 (width / 2, heigh / 2, 0);
			int[] triangels = { 0, 2, 1, 3, 1, 2 };
			Vector2[] uv = { new Vector2 (0, 0), new Vector2 (0, 1), new Vector2 (1, 0), new Vector2 (1, 1) };
			mesh.vertices = vertices;
			mesh.uv = uv;
			mesh.triangles = triangels;
			mesh.RecalculateNormals ();
			GetComponent<MeshFilter> ().sharedMesh = mesh;
		}
	}
	// Update is called once per frame
	void Update () {
		
	}
}
