﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class WindowDoor {
	//public List<Integrable> integrable;
	public Vector3 position;
	public float lenght;
	public float heigh;

	public Enums.TypeOfIntegrable type;
	public Enums.Orientation orientation;
	public Enums.Orientation2 orientation2;
	public bool inWall;
	public Wall parentWall;

	public WindowDoor(Vector3 position,float lenght, float heigh, Enums.TypeOfIntegrable type, Enums.Orientation orientation) 
	{
		this.position = position;
		this.lenght = lenght;
		this.orientation = orientation;
		this.heigh = heigh;
		this.type = type;
	}


}
