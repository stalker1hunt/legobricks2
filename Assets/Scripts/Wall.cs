﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Wall
{	
	public Vector3 position;
	public float lenght;
	public float heigh;
	public Enums.Orientation2 orientation2;
	public Enums.Orientation orientation;
	public List <WindowDoor> windowsAndDoors;
	public Fundament parentFundament;

	public Wall(Vector3 position,float lenght, float heigh, Enums.Orientation orientation) 
	{
		this.heigh = heigh;
		this.position = position;
		this.lenght = lenght;
		this.orientation = orientation;
		windowsAndDoors = new List<WindowDoor> ();
	}
}
