﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Calculations:MonoBehaviour {

    public static Calculations instance;

    public List<GameObject> walls = new List<GameObject>();
    public List<GameObject> windowsAndDoors = new List<GameObject>();
    public List<GameObject> fundaments = new List<GameObject>();

    public GameObject ErrorScreen;
    public Text errorText;
    public GameObject CalcScreen;

    public float volumeInCubicMetres;
	public float volumeInBricks;
	public float price;

    protected int bufInt;

    EventManager.EventWrapper showErrorText ;

    void OnEnable()
    {

        showErrorText = new EventManager.EventWrapper(delegate (MyEvent myEvent)
        {
            string txt = (string)myEvent.parameter;
            ErrorScreen.SetActive(true);
            errorText.text = txt;
        });
        EventManager.instance.Listen(EventManager.SHOW_ERROR_TEXT, showErrorText);
    }
    void OnDisable()
    {
        EventManager.instance.DestroyListener(EventManager.SHOW_ERROR_TEXT, showErrorText);
    }

    void Awake()
    {
        if (instance == null)
            instance = this;     
    }

    bool CheckAvaliableToCalculate()
    {
        if (walls.Count == 0)
        {
            errorText.text = "Установите стены";
            return false;
        }
               
        foreach (GameObject wall in walls)
        {
            if (wall.transform.parent.gameObject.GetComponent<VisualFundament>() == null)
            {
                bufInt = wall.GetComponent<ObjectIdentity>().buildObj.myID;
                errorText.text = "Не все стены прикреплены \n к фундаменту";
                return false;
            }
            Shape shape = wall.GetComponent<Shape>();
            if (shape.isColliding)
            {
                errorText.text = "Устраните наслоение\nстен";
                return false;
            }
        }

        foreach (GameObject window in windowsAndDoors)
        {
            Shape shape = window.GetComponent<Shape>();
            if (shape.isColliding)
            {
                errorText.text = "Устраните наслоение\nокон или дверей";
                return false;
            }
        }

        foreach (GameObject fundament in fundaments)
        {
            Shape shape = fundament.GetComponent<Shape>();
            if (shape.isColliding)
            {
                errorText.text = "Устраните наслоение\nфундаментов";
                return false;
            }
        }
        return true;
    }

    public void Calculate()
	{

        if (!CheckAvaliableToCalculate())
        {
            CalcScreen.SetActive(false);
            ErrorScreen.SetActive(true);
            return;
        }

        CalcScreen.SetActive(true);
        volumeInCubicMetres =0;
		foreach(GameObject wall in walls)
		{
           volumeInCubicMetres += wall.GetComponent<VisualWall>().wall.heigh *  wall.GetComponent<VisualWall>().wall.lenght * Enums.wallDepth;
        }
		foreach(GameObject windowOrDoor in windowsAndDoors)
		{
			if (windowOrDoor.GetComponent<VisualWindowDoor>().windowDoor.parentWall!=null) {
				volumeInCubicMetres -= windowOrDoor.GetComponent<VisualWindowDoor>().windowDoor.heigh * 
									   windowOrDoor.GetComponent<VisualWindowDoor>().windowDoor.lenght*
									   Enums.wallDepth;
			}
		}
		volumeInBricks = Mathf.Round(volumeInCubicMetres/(.25f * .12f * .065f));
		price = volumeInBricks * Enums.price;

	}

    public void CheckDeletedObj(BuildObj buildObj , GameObject g)
    {
        if (buildObj.objType == ObjType.door || buildObj.objType == ObjType.window)
        {
            windowsAndDoors.Remove(g);
        }
        if (buildObj.objType == ObjType.wall)
        {
            walls.Remove(g);
        }
        if (buildObj.objType == ObjType.fundament)
        {
            fundaments.Remove(g);
        }
    }

    public void OnClickShowUnParrentWall()
    {
        EventManager.instance.FireEvent(EventManager.SHOW_UNPARRENT_WALL,bufInt);
    }

    public Calculations()
	{
		this.walls = new List<GameObject> ();
		this.windowsAndDoors = new List<GameObject> ();
	}

	//float ()
}
