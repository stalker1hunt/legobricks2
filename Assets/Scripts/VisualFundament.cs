﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

//
//
public class VisualFundament : Shape {

    protected float square = 0;
    [HideInInspector]
    public Fundament fundament;
	public Vector3[] vertices;
	public float width;
	public float lenght;
	int [] baseVerticles;
	public Floor floorParent;
	public List <Wall> walls;
    public float zedPosition;
    EventManager.EventWrapper updateMap;
    BoxCollider boxCollider;
    Vector3 centerOfCollider;

    public TextMesh textMesh;
    public MeshRenderer textMeshRenderer;

  

    protected new void Awake (){
        base.Awake();
        if (GetComponent<BoxCollider>() == null)
        {
            base.shapeCollider = (Collider)gameObject.AddComponent<BoxCollider>();
        }
        else
            base.shapeCollider = (Collider)gameObject.GetComponent<BoxCollider>();
        boxCollider = base.GetComponent<BoxCollider>();
    }

    protected new void Start()
    {
        zedPosition = -1;
        base.Start();
        GetStats();
    }

    void GetStats()
    {
        if (objId.isLoadFromSave)
        {
            fundament = new Fundament(objId.buildObj.width, objId.buildObj.length, objId.buildObj.height, Vector3.zero);
            SetVisualFundament();
        }
        else
        {
      
            fundament = new Fundament(Enums.defaultFundamentLenght, Enums.defaultFundamentWidth, Enums.defaultFundamentHeight, Vector3.zero);
        }
        Calculations.instance.fundaments.Add(gameObject);
        //if (GetComponent<MeshCollider>() == null)
        //{
        //    base.shapeCollider = (Collider)gameObject.AddComponent<MeshCollider>();
        //}
        //else
        //    base.shapeCollider = (Collider)gameObject.GetComponent<MeshCollider>();

    }

    void OnMouseDown(){
		base.InOnMouseDown ();
		if (base.uiHitted) {
			return;
		}
		fundament.parrentFloor = Manager.manager.activeFloor.GetComponent<VisualFloor> ().floor;
	}
	void OnMouseDrag()
	{   
		if (base.uiHitted) {
			return;
		}

		base.InOnMouseDrag (0);
	}
	public void OnMouseUp()
	{

		if (base.uiHitted) {
			return;
		}

		base.InOnMouseUp (0 , GridBinder.gridPoints_big, GridBinder.b_Cell);	
	//	transform.position = GridBinder.BindToGrid(transform.position);
		fundament.position = transform.position;

        objId.UpdatePos();

	}
	public void SetMesh()
	{
        zedPosition = -1;
        base.shapeMesh = new Mesh ();
		base.shapeMesh.name = "House base mesh";

		vertices = new Vector3[4];
		vertices [0] = new Vector3 ( -fundament.width/2, -fundament.lenght/2, zedPosition);
		vertices [1] = new Vector3 ( fundament.width/2, -fundament.lenght/2, zedPosition);
		vertices [2] = new Vector3 ( -fundament.width/2, fundament.lenght/2, zedPosition);
		vertices [3] = new Vector3 ( fundament.width/2, fundament.lenght/2, zedPosition);
		int[] triangels = {0,2,1,3,1,2};
		Vector2[] uv = {new Vector2 (0,0),new Vector2 (0,1),new Vector2 (1,0),new Vector2 (1,1)};
		SetMesh (vertices, uv, triangels);
        //MeshCollider mc = (MeshCollider)shapeCollider;
        // mc.sharedMesh = shapeMesh;
        centerOfCollider = new Vector3(0, 0, zedPosition);
        boxCollider.center = centerOfCollider;
        boxCollider.size = new Vector3(fundament.width-0.05f, fundament.lenght - 0.05f, .5f);

    }
    public void SetVisualFundament() {

        width = fundament.width;
        lenght = fundament.lenght;
        transform.position = fundament.position;
        SetMesh();
        //	SetShaderParametrs ();
        UpdateWallText();
        try
        {
            MenuManager.instance.UpdateSelectedObjStats(objId.buildObj);
        }
        catch { }    
    }
	public void SetShaderParametrs()
	{
		var rend = GetComponent<Renderer> ();
		rend.sharedMaterial.SetFloat ("_Step",1);
		rend.sharedMaterial.SetFloat ("_Width",20);
		rend.sharedMaterial.SetFloat ("_Heigh",20);
        rend.sharedMaterial.SetFloat("_LineWidth",Manager.currentMapLine);
	}

    void UpdateWallText()
    {
        square = fundament.width * fundament.lenght;
        textMesh.text = square.ToString("F1") + "м²";
        if (fundament.width < 1.4f)
        {
            if (fundament.width < fundament.lenght)
            {
                textMesh.transform.rotation = Quaternion.Euler(0, 0, 90);
            }
            else
            {
                textMesh.transform.rotation = Quaternion.Euler(0, 0, 0);
            }
        }
        else
        {
            textMesh.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }

    protected override void UpdateMeshEnable()
    {
        base.UpdateMeshEnable();
        textMeshRenderer.enabled = !textMeshRenderer.enabled;
    }

    protected override void SetMeshEnable()
    {
        base.SetMeshEnable();
        textMeshRenderer.enabled = true;
    }

}
