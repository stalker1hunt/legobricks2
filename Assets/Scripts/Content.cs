﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Content : MonoBehaviour {
	public GameObject content;
	public float itemWeight;
	RectTransform rect;
	void Awake(){
	rect = content.GetComponent<RectTransform>();
		rect.sizeDelta = new Vector2(content.transform.childCount * itemWeight,rect.sizeDelta.y); 
	}
	void OnMouseDown(){
		
	}
	void OnMouseDrag(){   

	}
	public void OnEndDrag(){
		StopAllCoroutines ();
		rect.localPosition =new Vector2(Mathf.Round(rect.localPosition.x/itemWeight)*itemWeight, rect.localPosition.y);
		Debug.Log (rect.localPosition);
		Debug.Log ("OnMouseUP");
	}

}
