﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class SaveProject : MonoBehaviour {
    public static SaveProject ProjectSaver;
    float health;
    float experience;
    void Awake()
    {
        //Load();
		if (ProjectSaver == null)
        {
            DontDestroyOnLoad(gameObject);
			ProjectSaver = this;
        }
		else if (ProjectSaver != this)
        {
            Destroy(gameObject);
        }
    }
    void OnApplicatinPause() {
        Save();
    }
    void OnDestroy() {
        Save();
    }
    void OnApplicationQuit()
    {
        
    }

public static void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");
        SaveData data = new SaveData();
//        for (int i = 0; i < GameManager.BOARD_SIZE* GameManager.BOARD_SIZE; i++)
//           
//                {
//                data.board [i] = GameManager.board.gameBoard[ArrayMaths.OneToTwo(i)[0], ArrayMaths.OneToTwo(i)[1]];
//                }
//        data.currentScore = GameManager.score;
//        data.topScore = GameManager.topScore;
        bf.Serialize(file, data);
        file.Close();
    }
    public static void Load()
    {
        if (File.Exists(Application.persistentDataPath + "/playerInfo.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();

            FileStream file = File.Open(Application.persistentDataPath + "/playerInfo.dat", FileMode.Open);
            SaveData data = (SaveData)bf.Deserialize(file);

            file.Close();
//            for (int i = 0; i < GameManager.BOARD_SIZE; i++)
//                for (int j = 0; j < GameManager.BOARD_SIZE; j++)
//                {
//              GameManager.board.gameBoard[i,j] = data.board[ArrayMaths.TwoToOne(i,j)];
//                }
//              GameManager.score = data.currentScore;
//            GameManager.topScore = data.topScore;
         //   health = data.health;
         //   experience = data.experience;
        }
    }
}




[Serializable]
class SaveData
{
	public VisualFundament[] Fundaments; 
	public Wall[] walls;
}
