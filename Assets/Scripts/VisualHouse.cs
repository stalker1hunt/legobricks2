﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


public class VisualHouse : MonoBehaviour {
	public House house;
	public List<GameObject> floors;
    public static VisualHouse instance;
    GameObject go;
    // Use this for initialization
    void Awake () {
        if (instance == null)
            instance = this;
        go = new GameObject("Build");
        go.tag = "Finish";
        floors = new List<GameObject>();
        // CreateFlour();                      // переделать , убрать из старта , оставить при создании нового проекта

    }


    public void UpdateFloursActivity()
    {
        if (floors.Count != 0)
        {
            if (floors.Count > 1)
            {
                for (int i = 0; i < floors.Count; i++)
                {
                    floors[i].SetActive(false);
                }
            }
            print("UpdateFloursActivity");
            floors[0].SetActive(true);
        }
    }

    public void CreateFlour()
    {
        if (floors.Count == 0)
        {
           
            GameObject flor1 = Instantiate(Manager.manager.floorPrefab);
            flor1.name = "Flor" + (floors.Count + 1);
            floors.Add(flor1);
            flor1.transform.parent = go.transform;

        }
    }

    public void OnAddFloorClick()
    {
        if (floors.Count < 6)
        {
            GameObject floorInst = Instantiate(Manager.manager.floorPrefab);
            floorInst.name = "Flor" + (floors.Count + 1);
            floors.Add(floorInst);
            Manager.manager.activeFloor.SetActive(false);
            Manager.manager.activeFloor = floorInst;
            Manager.manager.CurrentFlor = floors.Count - 1;
        }
    }

    public void OnClickDeleteFloor()
    {
        if (floors.Count > 1)
        {
            
            foreach (Transform child in Manager.manager.activeFloor.transform)
            {
                int childId = child.gameObject.GetComponent<ObjectIdentity>().buildObj.myID;
                EventManager.instance.FireEvent(EventManager.DELETE_OBJ, childId);
            }
            GameObject bufFloor = Manager.manager.activeFloor;
            int floorId = Manager.manager.activeFloor.GetComponent<ObjectIdentity>().buildObj.myID;
            EventManager.instance.FireEvent(EventManager.DELETE_OBJ, floorId);
            floors.Remove(Manager.manager.activeFloor);
            Manager.manager.CurrentFlor = 0;
            floors[0].SetActive(true);
            Destroy(bufFloor);
        }
        // 
    }

    public void CheckDeletedObj(BuildObj buildObj, GameObject g)
    {
        if (buildObj.objType == ObjType.flor)
        {
                floors.Remove(g);
                print("DELETED");
        }
    }

    public int ActiveNextFloor(int activeFloor)
	{
		if (activeFloor>=0&&activeFloor<floors.Count-1) {
           
            floors [activeFloor + 1].SetActive (true);
			floors [activeFloor].SetActive (false);
			return activeFloor + 1;
		}
		return activeFloor;
	}
	public int ActivePreviousFloor(int activeFloor)
	{
		if (activeFloor>0&&activeFloor<=floors.Count) {
            
            floors [activeFloor - 1].SetActive (true);
			floors [activeFloor].SetActive (false);
			return activeFloor - 1;
		}
		return activeFloor;
	}
	//public 
}
