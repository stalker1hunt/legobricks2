﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Fundament {

	public float width;
	public float heigh;
	public float lenght;
	public Vector3 position;
	public Floor parrentFloor;
	public List<Wall> walls;


	public Fundament (float width, float lenght, float heigh, Vector3 position)
	{
		this.width = width;
		this.heigh = heigh;
		this.lenght = lenght;
		this.position = position;
		walls = new List<Wall> ();
	}


}
