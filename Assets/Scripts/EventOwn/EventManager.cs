﻿using UnityEngine;


using System.Collections.Generic;
using System.Runtime.InteropServices;


public class EventManager 
{

    //UI
    public static string SHOW_ERROR_TEXT = "SHOW_ERROR_TEXT";
    public static string SET_ACTIVE_PANEL = "SET_ACTIVE_PANEL";
    public static string UPDATE_SELECTED_OBJECT_STATS = "UPDATE_SELECTED_OBJECT_STATS";
    // GAME
    public static string SHOW_UNPARRENT_WALL = "SHOW_UNPARRENT_WALL";
    public static string CHECK_PARENT_TO_CALC = "CHECK_PARENT_TO_CALC";
    public static string UPDATE_MAP_LINE = "UPDATE_MAP_LINE";
    public static string DELETE_MENU_OBJ = "DELETE_MENU_OBJ";
    public static string CLEAR_ALL_STAFF = "CLEAR_ALL_STAFF";
    public static string DELETE_OBJ = "DELETE_OBJ";
    public static string SAVE_OBJ = "SAVE_OBJ";
    public static string SAVE_OBJ_LOCAL = "SAVE_OBJ_LOCAL";


    public static string PARAM_SOURCE = "PARAM_SOURCE";
	public static string PARAM_VALUE = "PARAM_VALUE";
	public static string PARAM_ACTION = "PARAM_ACTION";

        
	public  class EventWrapper
	{
			public EventWrapper (OnEvent onEvent)
			{
					this.onEvent = onEvent;
			}




			public OnEvent onEvent;

			public delegate void OnEvent (MyEvent myEvent);
	}


	public static EventManager instance = new EventManager ();


	public Dictionary<string, List<EventWrapper>> listeners = new Dictionary<string, List<EventWrapper>> ();

	void Dispatch (MyEvent customEvent)
	{
			List<EventWrapper> tempList = new List<EventWrapper> ();
			tempList.AddRange (listeners [customEvent.type]);

			foreach (EventWrapper listener in tempList) {
					listener.onEvent (customEvent);
			}

	}


	void AddListener (string type, EventWrapper listener)
	{
			if (!listeners.ContainsKey (type)) {
					listeners.Add (type, new List<EventWrapper> ());
			}
			listeners [type].Add (listener);

	}

	public void DestroyAllListeners (string type)
	{
			if (listeners.ContainsKey (type)) {
					listeners [type].Clear ();
			}

	}


	void RemoveListener (string type, EventWrapper wrapper)
	{
			if (listeners.ContainsKey (type)) {
					listeners [type].Remove (wrapper);
			}

	}

	public void FireEvent (string type, object parameter)
	{
			if (listeners.ContainsKey (type)) {
					MyEvent event1 = new MyEvent (type, parameter);
					Dispatch (event1);
			}
	}

	public void FireEvent (string type)
	{
			FireEvent (type, null);
	}



	public void Listen (string type, EventWrapper handler)
	{

			AddListener (type, handler);

	}


	public void DestroyListener (string type, EventManager.EventWrapper wrapper)
	{
			if (wrapper != null) {
			
					RemoveListener (type, wrapper);
			} else {
					Debug.Log ( "Null listener for destroy type ");
			}

	}


}
