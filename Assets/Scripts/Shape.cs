﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

//[RequireComponent(typeof(MeshFilter),typeof(MeshRenderer))]
public class Shape : MonoBehaviour {
    [HideInInspector]
    public ObjectIdentity objId;
    protected MeshRenderer meshRenderer;
    protected Mesh shapeMesh;
    public Collider shapeCollider;
    public bool uiHitted = false;
    int fingerID = -1;
    protected Vector3 onStartDraggingCursorPosition;
    protected float onStartDraggingTime;
    protected bool canRun;
    protected Vector3 mouseOffset;

    public bool isStart;
    public bool isColliding = false;
    [SerializeField]
    protected bool isMoved = false;


    public bool IsMoved 
    {
        get { return isMoved; }
    }

	protected void SetMesh(Vector3[] vertices, Vector2[] uv, int[]triangels)
	{
		shapeMesh.vertices = vertices;
		shapeMesh.uv = uv;
		shapeMesh.triangles = triangels;
		shapeMesh.RecalculateNormals();
		MeshFilter mf = GetComponent<MeshFilter> ();
		mf.sharedMesh = shapeMesh;
	}
    // Use this for initialization
    protected void Awake () 
	{
       

#if !UNITY_EDITOR
		fingerID = 0;
#endif
        if (shapeMesh == null)
            shapeMesh = new Mesh();
        shapeMesh.vertices = new Vector3[4];
        shapeMesh.uv = new Vector2[4];
        shapeMesh.triangles = new int[6];
        if (shapeCollider == null)
        {
            shapeCollider = new Collider();
        }
    }

    protected void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        objId = GetComponent<ObjectIdentity>();
      //  StartCoroutine(MyStart());
    }

    //IEnumerator MyStart()
    //{
    //    isStart = true;
    //    yield return new WaitForSeconds(0.5f);
    //    isStart = false;
    //}

    protected void LightingsMesh()
    {
       
       if (!isMoved)
        {
          
                UpdateMeshEnable();
          
        }
    }

    protected void OnCollisionEnter(Collision collisionInfo)
    {
        //if (gameObject == Manager.manager.selectedObject)
        //    isColliding = true;

        //if (isStart)
        //{
        //    if (collisionInfo.transform.position.x > transform.position.x)
        //    {
        //        isMoved = false;
        //        print("isMoved = TRUE X");
        //    }
        //    else
        //    {
        //        if (collisionInfo.transform.position.x == transform.position.x)
        //        {
        //            if (collisionInfo.transform.position.y > transform.position.y)
        //            {
        //                isMoved = false;
        //                print("isMoved = TRUE Y");
        //            }
        //        }
        //    }
        //}
    }

    protected void OnCollisionStay(Collision collisionInfo)
    {
        isColliding = true;
        if (collisionInfo.collider.tag == gameObject.tag)
        {    
            LightingsMesh();
        }      
    }
    protected void OnCollisionExit(Collision collisionInfo)
    {
       // isMoved = true;
        isColliding = false;
        SetMeshEnable();
    }

    protected void InOnMouseDown(){
        isMoved = true;
        print("InOnMouseDown");
        SetMeshEnable();
       // isStart = false;
        //Manager.manager.HideAllParametsWindows ();
        //Manager.manager.selectedObject = gameObject;
        if (EventSystem.current.IsPointerOverGameObject(fingerID))
		{
			uiHitted = true;
			return;
		}
		uiHitted = false;
		Vector3 mousePosition = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0);
		//Vector3 cursorPosition = Camera.main.ScreenToWorldPoint (mousePosition);
		mouseOffset = Camera.main.ScreenToWorldPoint(mousePosition) - transform.position;
		onStartDraggingCursorPosition = mousePosition;
		onStartDraggingTime = Time.time;
		canRun = true;
        Manager.manager.selectedObject = gameObject;

        MenuManager.instance.UpdateSelectedObjStats( objId.buildObj);
    }
	protected void InOnMouseDrag(float zedPosition)
	{
		Vector3 mousePosition = new Vector3 (Input.mousePosition.x, Input.mousePosition.y, 0);
		Vector3 objPosition = Camera.main.ScreenToWorldPoint (mousePosition);
		if (mousePosition != onStartDraggingCursorPosition) {
			transform.position = new Vector3 (objPosition.x - mouseOffset.x, objPosition.y - mouseOffset.y, zedPosition); //objPosition;
		} else {
			if (Time.time - onStartDraggingTime >= Enums.timeToShowParametrs) {
				if (canRun) {	
					//GameObject managerGo = GameObject.Find ("Manager");
				//	Manager.manager.GetComponent<Manager> ().selectedObject = gameObject;
				//	Manager.manager.GetComponent<Manager> ().ParamWindow.GetComponent<RectTransform> ().position = onStartDraggingCursorPosition;
				//	Manager.manager.GetComponent<Manager> ().ParamWindow.SetActive (true);
					canRun = false;
				}
			}
		}
	}
	protected virtual void InOnMouseUp(float zedPosition , Vector3[,] gridList, float _cellS)
	{
        isMoved = isColliding?false:true;
        transform.position = GridBinder.BindToPos(transform.position);//GridBinder.BindToGrid(transform.position, gridList , _cellS);
		transform.position = new Vector3 (transform.position.x,transform.position.y,zedPosition);


	}

    protected virtual void UpdateMeshEnable()
    {
        meshRenderer.enabled = Manager.IsShow ;//!meshRenderer.enabled;
    }

    protected virtual void SetMeshEnable()
    {
        meshRenderer.enabled = true;
    }
}
