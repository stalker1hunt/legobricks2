﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public static class Enums {

    public static float defaultFundamentLenght = 10f;
    public static float defaultFundamentWidth = 10f;
    public static float defaultFundamentHeight = 0.8f;
    public static float defaultWallLenght=5f;
	public static float defaultWallHeight=3f;
	public static float defaultWindowLenght=0.8f;
	public static float defaultWindowlHeight=1.4f;
    public static float defaultDoorLenght = 0.9f;
    public static float defaultDoorlHeight = 2f;
    public static float depth = 0.16f; 
	public static float cellSize = .1f;
	public static int xLenght = 200;
	public static int yLenght = 200;
	public static float price = 10;//RUB
	public static float wallDepth = .12f;
	public static float timeToShowParametrs = 1f;


	public enum Orientation {horizontal,vertical}
	public enum BlockType {fundamental,integrable,fundament}//{wall=0,window,arche,door,partition,colon,gate,roof}
	public enum Orientation2 {top=0, right, bottom, left}
	public enum Tool {select, spawn}
	public enum TypeOfIntegrable{door=0,window}

	public static List<string> ToStringList<T>()
	{
		return Enum.GetNames (typeof(T)).ToList();
	}
}
