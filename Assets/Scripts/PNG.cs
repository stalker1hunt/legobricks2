﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class PNG : MonoBehaviour {
	public Texture2D txt; 
	Texture2D MyPng;
	public GameObject go;

	// Use this for initialization
	void Start () {
		MyPng = new Texture2D (128, 128, TextureFormat.RGBA32, false);
		Draw ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void Draw()
	{
		
		for (int x = 0; x < MyPng.width; x++)
		for (int y = 0; y < MyPng.height; y++) {
				
				MyPng.SetPixel (x,y,Color.white);
				if(x==3)
				MyPng.SetPixel (x, y, Color.green);	
				if(x==128-y)
				MyPng.SetPixel (x, y, Color.red);	
				
			}
		MyPng.name = "SavedTexture";
		MyPng.Apply();
		go.GetComponent<Renderer>().material.mainTexture = MyPng;

		var toPng = MyPng.EncodeToPNG ();

		File.WriteAllBytes ("/SavedPNG.png",toPng);

	}
}
