﻿using UnityEngine;
using System.Collections;

public class HiResScreenShots : MonoBehaviour
{

    private static HiResScreenShots _instance;
    public static HiResScreenShots Instance
    { get { return _instance; } }

    public GameObject resultWindow;
    public int resWidth = 512;
    public int resHeight = 512;

   
    private bool takeHiResShot = false;
    public Camera _camera;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        resultWindow.SetActive(false);
    }

    public static string ScreenShotName(int width, int height , string name)
    {
        string pathFile;
#if UNITY_EDITOR
        pathFile = Application.dataPath + "/StreamingAssets/";
#endif
#if UNITY_ANDROID && !UNITY_EDITOR
        //pathFile = Application.persistentDataPath+"/";
        pathFile =  Application.persistentDataPath+"/";
#endif
        string[] result = name.Split('.');
        name = result[0];
        return string.Format("{0}/" +name +".png",
                             pathFile,
                             width, height);
    }


    public static string ScreenShotGlobal(int width, int height, string name)
    {
        string pathFile;
#if UNITY_EDITOR
        pathFile = "D:\\";
#endif
#if UNITY_ANDROID && !UNITY_EDITOR
        pathFile =  "storage/emulated/0/DCIM/" ;
#endif
        string[] result = name.Split('.');
        name = result[0];
        return string.Format("{0}/" + "LegoBricks" + System.DateTime.Now.ToString("yyyy_MM_dd_HH_mm") + ".png",
                             pathFile,
                             width, height);
    }

    void LateUpdate()
    {
        takeHiResShot |= Input.GetKeyDown("k");
        if (takeHiResShot)
        {
            TakeScreentShot("", Screen.width, Screen.height);
        }
    }

    public void TakeScreentShot(string name)
    {
        RenderTexture rt = new RenderTexture(resWidth, resHeight, 24);
        _camera.targetTexture = rt;
        Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.RGB24, false);
        _camera.Render();
        RenderTexture.active = rt;
        screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
        _camera.targetTexture = null;
        RenderTexture.active = null; // JC: added to avoid errors
        Destroy(rt);
        byte[] bytes = screenShot.EncodeToPNG();
        string filename = ScreenShotName(resWidth, resHeight , name);
        System.IO.File.WriteAllBytes(filename, bytes);
        Debug.Log(string.Format("Took screenshot to: {0}", filename));
        takeHiResShot = false;
    }

    public void TakeScreentShot(string name, int width, int length)
    {
        try
        {
             RenderTexture rt = new RenderTexture(width, length, 24);
             _camera.targetTexture = rt;
             Texture2D screenShot = new Texture2D(width, length, TextureFormat.RGB24, false);
             _camera.Render();
             RenderTexture.active = rt;
             screenShot.ReadPixels(new Rect(0, 0, width, length), 0, 0);
             _camera.targetTexture = null;
             RenderTexture.active = null; // JC: added to avoid errors
             Destroy(rt);
             byte[] bytes = screenShot.EncodeToPNG();
             string filename = ScreenShotGlobal(width, length, name);
             System.IO.File.WriteAllBytes(filename, bytes);
             Debug.Log(string.Format("Took screenshot to: {0}", filename));
             takeHiResShot = false;
         //   ScreenCapture.CaptureScreenshot("ScreenShot.png",2);
            resultWindow.SetActive(true);
        }
        catch { }
    }
}