﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadLogo : MonoBehaviour {


    public List<GameObject> loadScreens;

	IEnumerator Start () {

        foreach (GameObject g in loadScreens)
        {
            g.SetActive(false);
        }
        yield return new WaitForSeconds(0.1f);
        foreach (GameObject g in loadScreens)
        {
            g.SetActive(true);
            yield return new WaitForSeconds(0.5f);
        }
        yield return new WaitForSeconds(1.85f);
        MenuManager.instance.OnClickChangeScreen(1);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
