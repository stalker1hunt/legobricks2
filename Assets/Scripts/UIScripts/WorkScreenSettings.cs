﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorkScreenSettings : MonoBehaviour {


    public InputField projectName;
    public Slider slider;
    public Slider sliderVolume;

    public Image soundSliderImg;
    public Image netSliderImg;

    private string _fbShareLink = "https://www.facebook.com/sharer/sharer.php?u=";
    private string _twitterShareLink = "https://twitter.com/home?status=";
    private string _gplusShareLink = "https://plus.google.com/share?url=";

    public string appLink;

    public GameObject mapNet;

    void Start () {

        float volume = 0;
        float net = 0;
        SaveManager.Instance.LoadSettings(ref volume, ref net);
        slider.value = net;           
        sliderVolume.value = volume;
        SetMusicSliderColor();
        AudioHelper.Instance.UpdateSoundActivity(sliderVolume.value);
        SliderOnValueChanged();
    }
	

	void Update () {
        projectName.text = MenuManager.currentOpenedProj;
    }

    protected void SaveSettings()
    {
        SaveManager.Instance.SaveSettings(sliderVolume.value, slider.value);
    }

    void SetMusicSliderColor()
    {
        if (sliderVolume.value == 0)
        {
            soundSliderImg.color = new Color(195/255.0f,195 / 255.0f, 195 / 255.0f);
        }
        else
        {
            soundSliderImg.color = new Color(254 / 255.0f, 79 / 255.0f, 79 / 255.0f);
        }
    }

    public void SliderOnValueChanged()
    {
        if (slider.value == 0)
        {
            mapNet.SetActive(false);
            netSliderImg.color = new Color(195 / 255.0f, 195 / 255.0f, 195 / 255.0f);
        }
        else
        {
            mapNet.SetActive(true);
            netSliderImg.color = new Color(254 / 255.0f, 79 / 255.0f, 79 / 255.0f);
        }
        SaveSettings();
    }

    public void SliderOnVolumeChanged()
    {
        SetMusicSliderColor();
        AudioHelper.Instance.UpdateSoundActivity(sliderVolume.value);
        SaveSettings();
    }

    public void OnClickShareFB()
    {
        string url = _fbShareLink + appLink;
        Application.OpenURL(url);
    }
    public void OnClickShareTwitter()
    {
        string url = _twitterShareLink + appLink;
        Application.OpenURL(url);
    }
    public void OnClickShareGPlus()
    {
        string url = _gplusShareLink + appLink;
        Application.OpenURL(url);
    }
    public void OnclickShareVk()
    {
        Application.OpenURL("http://m.vk.com/share.php?url=" + appLink
        + "&title=" + "Я люблю кирпичи" + "&description=" + "Кирпичный завод ВанЛав");
    }
    public void OnClickSAvePng()
    {
        HiResScreenShots.Instance.TakeScreentShot("", 1280, 800);
    }
}
