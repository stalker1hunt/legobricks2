﻿using UnityEngine;
using System.Collections;

[SerializeField]
public enum MenuState
{
    Logo = 0,
    Menu = 1,
    Delete = 2,
    Parameters = 3,
    Help = 4 ,
    Info= 5 ,
    Work=6, 
    Settings=7, 
    Order = 8
}

public class PanelActivityController : MonoBehaviour {

    public MenuState panel;
    protected GameObject content;
    EventManager.EventWrapper setPanelActivity;
    bool isActive = false;
    void OnEnable()
    {
        setPanelActivity = new EventManager.EventWrapper(delegate (MyEvent myEvent)
        {
            MenuState sentPanel = (MenuState)myEvent.parameter;
            UpdatePanelActivity(sentPanel);
        });
        EventManager.instance.Listen(EventManager.SET_ACTIVE_PANEL, setPanelActivity);

    }
    void OnDisable()
    {
        EventManager.instance.DestroyListener(EventManager.SET_ACTIVE_PANEL, setPanelActivity);
    }

    void Start()
    {

        content = transform.GetChild(0).gameObject;
        if (panel == MenuState.Logo)
        {
            content.SetActive(true);            
        }
        else
        {
            content.SetActive(false);
        }
    }

    void UpdatePanelActivity(MenuState sentPanel)
    {
        if (panel == MenuState.Menu)
        {
            if (sentPanel == MenuState.Delete)
            {
                isActive = false;
                return;
            }
        }

        if (isActive == false)
        {
            if (sentPanel == panel)
            {
                content.SetActive(true);
                isActive = true;
            }
            else
            {
                content.SetActive(false);
                isActive = false;
            }
        }
        else
        {
            content.SetActive(false);
            isActive = false;
        }
    }
}
