﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
public class WindowDoorParamWindow : MonoBehaviour
{
//	public Dropdown typeOfInegrable;
//	public InputField position;
	public InputField lenght;
	public InputField heigh;
	//public Text title;
	[HideInInspector]
	public GameObject selectedObject;

	public void OnOkClick()
	{
        if (!string.IsNullOrEmpty(lenght.text))
            if (float.Parse(lenght.text) > 15 || float.Parse(lenght.text) < 0)
            {
                EventManager.instance.FireEvent(EventManager.SHOW_ERROR_TEXT, "Значение слишком большое\nили отрицательное");
                return;
            }

        if (!string.IsNullOrEmpty(heigh.text))
            if (float.Parse(heigh.text) > 15 || float.Parse(heigh.text) < 0)
            {
                EventManager.instance.FireEvent(EventManager.SHOW_ERROR_TEXT, "Значение слишком большое\nили отрицательное");
                return;
            }

        var selObj= selectedObject.GetComponent<VisualWindowDoor>();

		if (!string.IsNullOrEmpty (heigh.text))
			selObj.windowDoor.heigh = float.Parse (heigh.text);
		
		//if (!string.IsNullOrEmpty (heigh.text)) 
		//	selObj.windowDoor.heigh = float.Parse (heigh.text);

		if (!string.IsNullOrEmpty (lenght.text))
		    selObj.windowDoor.lenght = float.Parse (lenght.text);

	//	selObj.windowDoor.type = (Enums.TypeOfIntegrable)typeOfInegrable.value;
		selObj.SetWindowDoor();
        selObj.objId.UpdateStats();
        MenuManager.instance.UpdateSelectedObjStats(selObj.objId.buildObj);

        ClearFields ();
		gameObject.SetActive (false);
	}
	public void OnCancelClick()
	{
		ClearFields ();
		gameObject.SetActive (false);
	}
	public void ClearFields()
	{
		heigh.text = null;
		lenght.text = null;
	}

}

