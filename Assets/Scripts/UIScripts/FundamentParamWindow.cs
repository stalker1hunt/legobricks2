﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FundamentParamWindow : MonoBehaviour {

	public InputField heigh;
	public InputField lenght;
	public InputField width;
	//public Text title;
	[HideInInspector]
	public GameObject selectedObject;
	public void OnOkClick()
	{
        if (!string.IsNullOrEmpty(lenght.text))
            if (float.Parse(lenght.text) > 20 || float.Parse(lenght.text) < 0)
            {
                EventManager.instance.FireEvent(EventManager.SHOW_ERROR_TEXT, "Значение слишком большое\nили отрицательное");
                return;
            }
        if (!string.IsNullOrEmpty(heigh.text))
            if (float.Parse(heigh.text) > 2 || float.Parse(heigh.text) < 0)
            {
                EventManager.instance.FireEvent(EventManager.SHOW_ERROR_TEXT, "Значение слишком большое\nили отрицательное");
                return;
            }
        if (!string.IsNullOrEmpty(width.text))
            if (float.Parse(width.text) > 20 || float.Parse(width.text) < 0)
            {
                EventManager.instance.FireEvent(EventManager.SHOW_ERROR_TEXT, "Значение слишком большое\nили отрицательное");
                return;
            }

        var selObj= selectedObject.GetComponent<VisualFundament> ();
		if (!string.IsNullOrEmpty (lenght.text))
			selObj.fundament.lenght = float.Parse(lenght.text);
		if (!string.IsNullOrEmpty (heigh.text))
			selObj.fundament.heigh = float.Parse(heigh.text);
		if (!string.IsNullOrEmpty (width.text))
			selObj.fundament.width = float.Parse(width.text);

	    selObj.objId.UpdateStats();
		selObj.SetVisualFundament();
        MenuManager.instance.UpdateSelectedObjStats(selObj.objId.buildObj);
        ClearFields ();
		gameObject.SetActive (false);
	}
	public void OnCancelClick()
	{
		ClearFields ();
		gameObject.SetActive (false);
	}
	public void ClearFields()
	{
		heigh.text = null;
		lenght.text = null;
		width.text = null;
	}

}
