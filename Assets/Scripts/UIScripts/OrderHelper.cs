﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Net.Mail;
using System.Net;
using System.ComponentModel;

public class OrderHelper : MonoBehaviour {
    public static OrderHelper instance;


    public int countOfBricks = 0;
    [Header("Input Fields")]
    public InputField tbName;
    public InputField tbNumber;
    public InputField tbEmail;
    public InputField tbBrickCount;

    [Header("Error Message")]
    public GameObject errorScreen;
    public Text errorText;
    public GameObject loadingPanel;
    
    void Awake()
    {
        instance = this;
    }

    void Start()
    {
        errorScreen.SetActive(false);
        loadingPanel.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
         //   SentMail();
        }
    }

    #region OnClick
    public void OnClickOrder()
    {
        Order();
    }

    public void OnClickGoToOrder()
    {
        tbBrickCount.text = Calculations.instance.volumeInBricks.ToString();
        countOfBricks = (int)Calculations.instance.volumeInBricks;
        EventManager.instance.FireEvent(EventManager.SET_ACTIVE_PANEL, MenuState.Order);
    }

    #endregion

    protected void Order()
    {
        if (!CheckError())
        {
            return;
        }



        List<string> orderList = new List<string>();

        // Имя, Фамилия , Отчество
        // Номер телефона 
        // Email
        // Количество кирпича
        countOfBricks = (int)Int32.Parse(tbBrickCount.text);
        orderList.Add("Имя: " + tbName.text);
        orderList.Add("Телефон: " + tbNumber.text);
        orderList.Add("Email: " + tbEmail.text);
        orderList.Add("К-во кирпича: " + countOfBricks);

        string body = "";
        foreach (string str in orderList)
        {
            body += str + "\n";
        }

        SentMail(body);
    }


    private bool CheckError()
    {
        if (String.IsNullOrEmpty(tbEmail.text))
        {
            ShowErrorText("Вы не ввели Email адрес", "Внимание!");
            return false;
        }
        else
        {
            if (!tbEmail.text.Contains("@"))
            {
                ShowErrorText("Введите корректный Email адрес", "Внимание!");
                return false;
            }
        }

        if (String.IsNullOrEmpty(tbName.text))
        {
            ShowErrorText("Вы не ввели\nимя и фамилию", "Внимание!");
            return false;
        }
        else
        {
            if (!tbName.text.Contains(" "))
            {
                ShowErrorText("Введите имя и фамилию", "Внимание!");
                return false;
            }
        }

        if (String.IsNullOrEmpty(tbNumber.text))
        {
            ShowErrorText("Вы не ввели номер телефона", "Внимание!");
            return false;
        }
        return true;
    }

    protected void ShowErrorText(string textBody, string textHeader)
    {
        errorText.text = textBody;
     //   errorTextHeader.text = textHeader;
        errorScreen.SetActive(true);
    }


    public void SentMail(string body)
    {

        loadingPanel.SetActive(true);

        string my_email = "koschinscy@yandex.ru";
        string pass = "qwerty1234";
        string email_recipient = "fortestmailsender39@gmail.com";

        SimpleEmailSender.emailSettings.STMPClient = "smtp.yandex.ru";
        SimpleEmailSender.emailSettings.SMTPPort = 587;//465;
        SimpleEmailSender.emailSettings.UserName = my_email;
        SimpleEmailSender.emailSettings.UserPass = pass;

        SimpleEmailSender.Send(email_recipient, "Order of building brick", body , SendCompletedCallback);
    }


    private void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
    {
        loadingPanel.SetActive(false);
        if (e.Cancelled || e.Error != null)
        {
            print("Email not sent: " + e.Error.ToString());
            ShowErrorText("Сообщение не отправлено\nОбратитесь в службу поддержки", "Внимание!");
        }
        else
        {
            print("Email successfully sent.");
            ShowErrorText("Сообщение отправлено\nСпасибо", "Отправлено");
        }
    }
}
