﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuParametersScreen : MonoBehaviour {


    public string commentURL = "";
    public string rateUsURL = "";
    public string siteURL = "";


    public void OpenUrl(string str)
    {
        Application.OpenURL(str);
    }

    public void OnClickOpenComment()
    {
        OpenUrl(commentURL);
    }
    public void OnClickOpenRateUs()
    {
        OpenUrl(rateUsURL);
    }
    public void OnClickOpenSite()
    {
        OpenUrl(siteURL);
    }   

}
