﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CalculationWindow : MonoBehaviour {
	public Text volumeInCubicMetresLabel;
	public Text volumeInBricksLabel;
	public Text price;

	public void OnCancelClick()
	{
		gameObject.SetActive (false);
	}
	public void OnOkClick()
	{
		gameObject.SetActive (false);
	}
}
