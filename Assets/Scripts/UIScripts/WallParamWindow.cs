﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class WallParamWindow : MonoBehaviour {
	
	public InputField heigh;
	public InputField lenght;
	public Dropdown orientation;
//	public Text title;
	[HideInInspector]
	public GameObject selectedObject;

	void Awake()
	{
		//selectedObject = new GameObject ();
	}
	public void OnOkClick()
	{
        if (!string.IsNullOrEmpty(lenght.text))
            if (float.Parse(lenght.text) > 20 && float.Parse(lenght.text) < 0)
            {
                EventManager.instance.FireEvent(EventManager.SHOW_ERROR_TEXT, "Значение слишком большое\nили отрицательное");
                return;
            }

        if (!string.IsNullOrEmpty(heigh.text))
            if (float.Parse(heigh.text) > 20 && float.Parse(heigh.text) < 0)
            {
                EventManager.instance.FireEvent(EventManager.SHOW_ERROR_TEXT, "Значение слишком большое\nили отрицательное");
                return;
            }

        var selObj = selectedObject.GetComponent<VisualWall>();
        if (!string.IsNullOrEmpty(lenght.text))
            selObj.wall.lenght = float.Parse(lenght.text);
        if (!string.IsNullOrEmpty(heigh.text))
            selObj.wall.heigh = float.Parse(heigh.text);
        //if (!string.IsNullOrEmpty (yCoord.text))
        //selectedObject.transform.position = new Vector3 ( float.Parse(xCoord.text),float.Parse(yCoord.text),selectedObject.transform.position.z);
        selObj.wall.orientation = (Enums.Orientation)orientation.value;
        selObj.UpdateWall();
        selObj.objId.UpdateStats();     // UPDATE STATS
        MenuManager.instance.UpdateSelectedObjStats(selObj.objId.buildObj);
        ClearFields();
        gameObject.SetActive(false);

        for (int i = 0; i < selObj.transform.childCount; i++)
        {
            if (selObj.transform.GetChild(i).GetComponent<VisualWindowDoor>() != null)
            {
                VisualWindowDoor tempVisualWindowDoor = selObj.transform.GetChild(i).GetComponent<VisualWindowDoor>();
                tempVisualWindowDoor.windowDoor.orientation = (Enums.Orientation)orientation.value;
                tempVisualWindowDoor.BindToWallGameObj(selObj.gameObject);
                tempVisualWindowDoor.SetWindowDoor();
            }
        }
	}
	public void OnCancelClick()
	{
		ClearFields ();
		gameObject.SetActive (false);
	}

	public void ClearFields()
	{
		heigh.text = null;
		lenght.text = null;
	}


}
