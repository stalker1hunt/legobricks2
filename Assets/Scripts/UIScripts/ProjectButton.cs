﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ProjectButton : MonoBehaviour {


    public SavedProject savedProj;

    public delegate void MyDelegate(SavedProject savedProj);
    public MyDelegate onClickProj;
    public Text nameText;
    public Image projImg;

    EventManager.EventWrapper clearAll;

    void OnEnable()
    {

        clearAll = new EventManager.EventWrapper(delegate (MyEvent myEvent)
        {
            SavedProject s = (SavedProject)myEvent.parameter;
            if (s.name == savedProj.name)
            {
                Destroy(gameObject);
            }
        });
        EventManager.instance.Listen(EventManager.DELETE_MENU_OBJ, clearAll);
      //  SetProjImage();
    }
    void OnDisable()
    { 
        EventManager.instance.DestroyListener(EventManager.DELETE_MENU_OBJ, clearAll);
    }


    void Start()
    {
        SetProjName();
       // SetProjImage();
    }

    public void OnClickProj()
    {
        CameraMovement.instance.DropDownCamSize();
        onClickProj(savedProj);
    }

    protected void SetProjName()
    {
        string[] result = savedProj.name.Split('.');
        nameText.text = result[0];
    }

    public void SetProjImage()
    {
        try
        {
            string pathFile;
#if UNITY_EDITOR
            pathFile = Application.dataPath + "/StreamingAssets/";
#endif
#if UNITY_ANDROID && !UNITY_EDITOR
        pathFile = Application.persistentDataPath+"/";
#endif
            string[] result = savedProj.name.Split('.');
            string name = result[0];

            projImg.sprite = IMG2Sprite.Instance.LoadNewSprite(pathFile + "/" + name + ".png");

        }
        catch { }

        print("Обновление изображения" + gameObject.name + " " + transform.parent.gameObject.name);
    }
}
