using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//[RequireComponent(typeof(MeshFilter),typeof(MeshRenderer))]
public class VisualWall : Shape 
{
	float colliderSizeOffset =0.1f;
	public Enums.Orientation ori;
    [HideInInspector]
	public Wall wall;
	protected float zedPosition;
	public float wallLenght;
	public bool setLenght=false;
	Vector3 centerOfCollider;
	Vector3[] vertices;
	BoxCollider boxCollider;
    Renderer renderer;
    public TextMesh textMesh;
    public MeshRenderer textMeshRenderer;
    public Material horMat;
    public Material verMat;

    EventManager.EventWrapper check_parent_to_calc;
    EventManager.EventWrapper bad_parent;

    void OnEnable()
    {

        bad_parent = new EventManager.EventWrapper(delegate (MyEvent myEvent)
        {
            int buf = (int)myEvent.parameter;
            if (buf == objId.buildObj.myID)
            {
                StartCoroutine(ShowUnPattentWall());
            }
        });
        EventManager.instance.Listen(EventManager.SHOW_UNPARRENT_WALL, bad_parent);

    }
    void OnDisable()
    {
        EventManager.instance.DestroyListener(EventManager.SHOW_UNPARRENT_WALL, bad_parent);
    }


    //25 сантеметров длина ширина 12 
    public void AddIntegrableObject(float position, GameObject integrable)
	{
		
	}
	protected new void Awake()
	{
        renderer = transform.GetComponent<Renderer>();
        //zedPosition = -1f;
        base.shapeMesh = new Mesh ();
		base.shapeMesh.name = "Block";

		//boxCollider = new BoxCollider ();
		//boxCollider = GetComponent<BoxCollider>();
		if (GetComponent<BoxCollider>() == null) {
			base.shapeCollider = (Collider)gameObject.AddComponent<BoxCollider> ();
		}else
			base.shapeCollider = (Collider)gameObject.GetComponent<BoxCollider> ();

		vertices = new Vector3[4];
		boxCollider = base.GetComponent<BoxCollider> ();

        textMesh = GetComponentInChildren<TextMesh>();

    }


	void OnMouseDown(){
		base.InOnMouseDown();

		if (base.uiHitted) {
			return;
		}
        try
        {
            if (wall.parentFundament != null)
                wall.parentFundament.walls.Remove(wall);
        }
        catch { }
    }

	void OnMouseDrag()
	{	 
		if (base.uiHitted) {
			return;
		}
		base.InOnMouseDrag (zedPosition);

	}
	public void OnMouseUp()
	{
		if (base.uiHitted) {
			return;
		}
		CheckIfWePlaceItOnTheFundament ();
		base.InOnMouseUp (zedPosition, GridBinder.gridPoints_little, GridBinder.l_Cell);
		wall.position = transform.position;

        
        objId.UpdateParrent();
        objId.UpdatePos();
    }

    protected new void Start()
	{
        zedPosition = -2;
        base.Start();
        //boxCollider = GetComponent<BoxCollider> ();//new BoxCollider ();
        //base.shapeCollider = boxCollider;
        //boxCollider = (BoxCollider)base.shapeCollider;
        if (wall == null)
        {
            print("WALL ++ NULL");
            wall = new Wall(Vector3.zero, Enums.defaultWallLenght, Enums.defaultWallHeight, ori);
            SetWallVerticesMidle(wall);
        }
        if (objId.isLoadFromSave)
        {
            wall = new Wall(objId.buildObj.pos, objId.buildObj.length, objId.buildObj.height, objId.buildObj.ori);
            UpdateWall();
        }
        CheckIfWePlaceItOnTheFundament();
    }
	void Update()
	{
		if (setLenght) {
			SetWallLenght (wallLenght);
			setLenght = false;
		}
		//block.lenght = blockLenght;

	}

    IEnumerator ShowUnPattentWall()
    {
        yield return new WaitForSeconds(0.5f);
        UpdateMeshEnable();
        yield return new WaitForSeconds(0.5f);
        UpdateMeshEnable();
        yield return new WaitForSeconds(0.5f);
        UpdateMeshEnable();
        yield return new WaitForSeconds(0.5f);
        UpdateMeshEnable();
        yield return new WaitForSeconds(0.5f);
        UpdateMeshEnable();
        yield return new WaitForSeconds(0.5f);
        UpdateMeshEnable();
    }

	void SetWallVerticesMidle(Wall wall)
	{
        zedPosition = -2;
        switch (wall.orientation) {
		    case Enums.Orientation.vertical:
			{
				vertices [0] = new Vector3 (-Enums.depth, -wall.lenght / 2, zedPosition);
				vertices [1] = new Vector3 (+Enums.depth, -wall.lenght / 2, zedPosition);
				vertices [2] = new Vector3 (-Enums.depth, +wall.lenght / 2, zedPosition);
				vertices [3] = new Vector3 (+Enums.depth, +wall.lenght / 2, zedPosition);

				centerOfCollider = new Vector3 (0, 0, zedPosition);
				boxCollider.center = centerOfCollider;
				boxCollider.size = new Vector3 (Enums.depth * 2 - colliderSizeOffset * 0.5f, wall.lenght - colliderSizeOffset, .5f);
                renderer.sharedMaterial = verMat;
                float x = wall.lenght / 5.0f;
                renderer.material.mainTextureScale = new Vector2(x, 1);

                }
			break;
		
		    case Enums.Orientation.horizontal:
			{
				vertices [0] = new Vector3 (- wall.lenght / 2, - Enums.depth, zedPosition);
				vertices [2] = new Vector3 (- wall.lenght / 2, + Enums.depth, zedPosition);
				vertices [1] = new Vector3 (+wall.lenght / 2, -Enums.depth,  zedPosition);
				vertices [3] = new Vector3 (+wall.lenght / 2, +Enums.depth, zedPosition);

				centerOfCollider = new Vector3 (0, 0, zedPosition);
				boxCollider.center = centerOfCollider;
				boxCollider.size = new Vector3 (wall.lenght - colliderSizeOffset, Enums.depth * 2 - colliderSizeOffset * 0.5f,  .5f);
                renderer.sharedMaterial = horMat;
                float x = wall.lenght / 5.0f;
                renderer.material.mainTextureScale = new Vector2(1, x);
                }
			break;
		}

        UpdateWallText();
	}
	void SetWallVertices(Wall block)
	{
		switch (block.orientation2) {
		case Enums.Orientation2.top:
			{
				vertices [0] = new Vector3 ( - Enums.depth, 0, zedPosition);
				vertices [1] = new Vector3 ( + Enums.depth, 0, zedPosition);
				vertices [2] = new Vector3 ( - Enums.depth, +block.lenght, zedPosition);
				vertices [3] = new Vector3 ( + Enums.depth, +block.lenght, zedPosition);

				centerOfCollider = new Vector3 (0,+block.lenght/2, zedPosition/2);
				boxCollider.center = centerOfCollider;
				boxCollider.size = new Vector3 (Enums.depth*2 - colliderSizeOffset * 0.5f, block.lenght-colliderSizeOffset, .5f);

			}
			break;
		case Enums.Orientation2.bottom:
			{
				vertices [0] = new Vector3 ( - Enums.depth, -block.lenght, zedPosition);
				vertices [1] = new Vector3 ( + Enums.depth, -block.lenght, zedPosition);
				vertices [2] = new Vector3 ( - Enums.depth, 0, zedPosition);
				vertices [3] = new Vector3 ( + Enums.depth, 0, zedPosition);

				centerOfCollider = new Vector3 (0, - block.lenght/2, zedPosition/2);
				boxCollider.center = centerOfCollider;
				boxCollider.size = new Vector3 (Enums.depth*2 - colliderSizeOffset * 0.5f, block.lenght-colliderSizeOffset, .5f);
			
			}
			break;
		case Enums.Orientation2.right:
			{
				//lb
				vertices [0] = new Vector3 ( 0 ,           - Enums.depth, zedPosition);
				//rb
				vertices [1] = new Vector3 ( block.lenght, - Enums.depth, zedPosition);
				//lt
				vertices [2] = new Vector3 ( 0 ,             Enums.depth, zedPosition);
				//rt
				vertices [3] = new Vector3 ( block.lenght,   Enums.depth, zedPosition);

				centerOfCollider = new Vector3 (+block.lenght/2, 0, zedPosition/2);
				boxCollider.center = centerOfCollider;
				boxCollider.size = new Vector3 (block.lenght-colliderSizeOffset, Enums.depth*2 - colliderSizeOffset * 0.5f, .5f);

			}
			break;
		case Enums.Orientation2.left:
			{
				vertices [1] = new Vector3 (  0,			 - Enums.depth, zedPosition);//rb
				vertices [0] = new Vector3 (- block.lenght,  - Enums.depth, zedPosition);//lb
				vertices [3] = new Vector3 (  0,               Enums.depth, zedPosition);//rt
				vertices [2] = new Vector3 (- block.lenght,    Enums.depth, zedPosition);//lt


				centerOfCollider = new Vector3 (-block.lenght/2, 0, 0);
				boxCollider.center = centerOfCollider;
				boxCollider.size = new Vector3 (block.lenght-colliderSizeOffset, Enums.depth*2 - colliderSizeOffset*0.5f, .5f);
			}
			break;
		}
		//if(vertices!=null)
		//base.shapeMesh.vertices = vertices; 
		
	}

	void SetMesh()
	{	
		int[] triangels = {0,2,1,3,1,2};
		Vector2[] uv = {new Vector2 (0,0),new Vector2 (0,1),new Vector2 (1,0),new Vector2 (1,1)};
		base.SetMesh (vertices, uv, triangels);
	}
	public void UpdateWall()
	{
		
		vertices = new Vector3[4];
		//block = new Block(Vector3.zero, 4f,ori) ;
		SetWallVerticesMidle (wall);
		SetMesh ();
	}
	public void SetWallLenght(float lenght)
	{
		wallLenght = lenght;
		wall.lenght = lenght;
		UpdateWall();
	}

    public void CheckParentFundament()
    {
        if (wall.parentFundament == null)
        {
            if (transform.parent.gameObject.GetComponent<VisualFundament>() != null)
            {
                wall.parentFundament = transform.parent.gameObject.GetComponent<VisualFundament>().fundament;
            }
            else
                return;
        }
    }
    
	public void CheckIfWePlaceItOnTheFundament()
	{
		Ray landingRay = Camera.main.ScreenPointToRay (Input.mousePosition); 
		RaycastHit[] allHits;
		bool hittedFundament=false;
		allHits=Physics.RaycastAll (landingRay, 100);
		//TODO OPTIMISE THIS PART LATER
		foreach(var hit in allHits)
		{
			if (hit.collider.tag == "House Base") {

				transform.SetParent (hit.collider.transform);
				hittedFundament = true;
				wall.parentFundament = hit.transform.GetComponent<VisualFundament> ().fundament;
				wall.parentFundament.walls.Add (wall);
			//	windowDoor.parentWall = hit.collider.GetComponent<VisualWall> ().wall;
				break;		

			} 
		}
		if (!hittedFundament) {
			transform.SetParent (Manager.manager.transform.GetComponent<VisualHouse>().floors[Manager.manager.CurrentFlor].GetComponent<Transform>());
			wall.parentFundament = null;
		}
	}

    void UpdateWallText()
    {
        textMesh.gameObject.SetActive(false);
        //try
        //{
        //    MenuManager.instance.UpdateSelectedObjStats(objId.buildObj);
        //}
        //catch { }
        //textMesh.text = "Д: " + wall.lenght.ToString("F1") + "м В: " + wall.heigh.ToString("F1") + "м";

        //if (wall.lenght < 1.78f)
        //{
        //    if (wall.lenght < 1.2f)
        //    {
        //        textMesh.gameObject.SetActive(false);
        //    }
        //    else
        //    {
        //        textMesh.text = "Д: " + wall.lenght.ToString("F1") + "м";
        //    }
        //}
        //else
        //{
        //    textMesh.gameObject.SetActive(true);
        //}

        //if (wall.orientation == Enums.Orientation.vertical)
        //{
        //    textMesh.transform.rotation = Quaternion.Euler(0,0,90);
        //}
        //if (wall.orientation == Enums.Orientation.horizontal)
        //{
        //    textMesh.transform.rotation = Quaternion.Euler(0, 0, 0);
        //}
    }

    protected override void UpdateMeshEnable()
    {
        base.UpdateMeshEnable();
        textMeshRenderer.enabled = !textMeshRenderer.enabled;
    }
    protected override void SetMeshEnable()
    {
        base.SetMeshEnable();
        textMeshRenderer.enabled = true;
    }

}
