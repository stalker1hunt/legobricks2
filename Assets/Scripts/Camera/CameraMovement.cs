﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public static CameraMovement instance;

    public List<CameraPoint> minCamSizePos;
    public List<CameraPoint> maxCamSizePos;

    public Vector2 xPos;
    public Vector2 yPos;
    protected float speed = 0.03f;

    //[Header("Camera")]
    protected Camera _camera;

    private int maxCamSize = 10;
    private int minCamSize = 2;
    public float orthoZoomSpeed = 0.75f;
    public float zoomPercentage; //Controls zoom speed
    public GameObject workContent;
    bool isMove = false;
    public Vector3 lastPasition;

    void Awake()
    {
        instance = this;
    }

    public void Start()
    {
        transform.position = new Vector3(10,10,-10);
        _camera = GetComponent<Camera>();
        DropDownCamSize();
        UpdateCamPositions();
        lastPasition = transform.position;
    }

    public void Update()
    {

        ChangeCamSize();
        CameraMove();

    }

    public void DropDownCamSize()
    {
        _camera.orthographicSize = 7;
    }

    public void UpdateCamPositions()
    {

        float t = (_camera.orthographicSize - 2f) / 8f;
        xPos = new Vector2(Mathf.Lerp(minCamSizePos[0].v2.x, maxCamSizePos[0].v2.x, t),
                            Mathf.Lerp(minCamSizePos[1].v2.x, maxCamSizePos[1].v2.x, t));

        yPos = new Vector2(Mathf.Lerp(minCamSizePos[1].v2.y, maxCamSizePos[1].v2.y, t),
                            Mathf.Lerp(minCamSizePos[0].v2.y, maxCamSizePos[0].v2.y, t));

    }

    public void CameraMove()
    {
        if (workContent.active)
        {
            if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began)
          //  if(Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (!Physics.Raycast(ray, out hit, 100.0f))
                {
                    isMove = true;
                    print("isMove " +  isMove);
                }
            }
            if (isMove)
            {
                if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved)
                {

                     Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
                    //Vector2 touchDeltaPosition = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
                    lastPasition += new Vector3(-touchDeltaPosition.x * speed,
                       -touchDeltaPosition.y * speed,
                        0);
                    if (xPos.x < lastPasition.x && lastPasition.x < xPos.y)
                    {
                       if (yPos.x < lastPasition.y && lastPasition.y < yPos.y)
                        {
                            transform.position += new Vector3(  -touchDeltaPosition.x * speed,
                                                                -touchDeltaPosition.y * speed,
                                                                0);
                          
                       }
                     }
                    lastPasition = transform.position;
                }
            }

            if (Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Ended)
            //if (Input.GetMouseButtonUp(0))
            {
                isMove = false;
            }

        }
    }

    public void ChangeCamSize()
    {
        //#if UNITY_EDITOR
        //        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        //        {
        //            if (Camera.main.orthographicSize < maxCamSize)
        //                Camera.main.orthographicSize += 1;
        //        }
        //        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        //        {
        //            if (Camera.main.orthographicSize > minCamSize)
        //                Camera.main.orthographicSize -= 1;
        //        }
        //#endif
        //#if UNITY_ANDROID && !UNITY_EDITOR
        // If there are two touches on the device...
        zoomPercentage = (_camera.orthographicSize / 50.0f);

        if (Input.touchCount == 2)
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;


            _camera.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed * zoomPercentage;
            _camera.orthographicSize = Mathf.Max(_camera.orthographicSize, minCamSize);
            _camera.orthographicSize = Mathf.Min(_camera.orthographicSize, maxCamSize);

            UpdateCamPositions();
            CheckPositionAfterResize();
        }
        //#endif
    }

    void CheckPositionAfterResize()
    {       
        if (xPos.x > transform.position.x)
        {
            transform.position = new Vector3(xPos.x + 0.1f , transform.position.y , transform.position.z);
        }
        if (transform.position.x > xPos.y)
        {
            transform.position = new Vector3(xPos.y - 0.1f, transform.position.y, transform.position.z);
        }

        if (yPos.x > transform.position.y)
        {
            transform.position = new Vector3(transform.position.x, yPos.x + 0.1f, transform.position.z);
        }
        if (transform.position.y > yPos.y)
        {
            transform.position = new Vector3(transform.position.x, yPos.y - 0.1f, transform.position.z);
        }
    }
}

[System.Serializable]
public class CameraPoint
{
    public float size;
    public Vector2 v2;
}
