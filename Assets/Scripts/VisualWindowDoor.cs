using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//[RequireComponent(typeof(MeshFilter),typeof(MeshRenderer))]
public class VisualWindowDoor : Shape 
{
	float colliderSizeOffset =0.1f;
	public Enums.Orientation ori;
    [HideInInspector]
    public WindowDoor windowDoor;
	public Enums.TypeOfIntegrable typeOfObject;
	public float zedPosition;
	public float blockLenght;
	public bool setLenght=false;
	Vector3 centerOfCollider;
	Vector3[] vertices;
	BoxCollider boxCollider;
	Renderer renderer;
    public Material horMat;
    public Material verMat;

    void Awake()
	{	
		renderer = transform.GetComponent<Renderer> ();
	//	zedPosition = -1.1f;
		base.shapeMesh = new Mesh ();
		base.shapeMesh.name = "Block";

		if (GetComponent<BoxCollider>() == null) {
			base.shapeCollider = (Collider)gameObject.AddComponent<BoxCollider> ();
		}else
			base.shapeCollider = (Collider)gameObject.GetComponent<BoxCollider> ();
		vertices = new Vector3[4];
		boxCollider = base.GetComponent<BoxCollider> ();
	}

    void OnMouseDown() {
        base.InOnMouseDown();
        if (base.uiHitted) {
            return;
        }
        //remove door from wall's list
        try
        {
            if (windowDoor.parentWall != null)
                windowDoor.parentWall.windowsAndDoors.Remove(windowDoor);
        }
        catch { }
    }

	void OnMouseDrag(){	
		if (base.uiHitted) {
			return;
		}
		base.InOnMouseDrag (zedPosition);
	}

	public void OnMouseUp(){

        isMoved = isColliding ? false : true;

        print("OnMouseUp");
        if (base.uiHitted) {
			return;
		}
		CheckIfWePlaceItOnTheWall ();

        // EventManager.instance.FireEvent(EventManager.UPDATE_PARRENTS);
        objId.UpdateParrent();
        objId.UpdatePos();
    }

	protected new void Start(){
        zedPosition = -3;
        base.Start();
        //boxCollider = GetComponent<BoxCollider> ();//new BoxCollider ();
        //base.shapeCollider = boxCollider;
        //boxCollider = (BoxCollider)base.shapeCollider;
       
        if (windowDoor == null)
        {
            windowDoor = new WindowDoor(Vector3.zero, Enums.defaultWindowLenght, Enums.defaultWindowlHeight, Enums.TypeOfIntegrable.window, ori);
            SetVerticesMidle(windowDoor);
           
        }
        if (objId.isLoadFromSave)
        {
            windowDoor = new WindowDoor(objId.buildObj.pos,
                objId.buildObj.length,
                objId.buildObj.height,
                (objId.buildObj.objType == ObjType.door ? Enums.TypeOfIntegrable.door : Enums.TypeOfIntegrable.window)
                , objId.buildObj.ori);
            SetWindowDoor();
        }
        else
        {
            Calculations.instance.windowsAndDoors.Add(gameObject);
        }
        SelectType();
        //SetMesh ();
    }
	void Update(){
		if (setLenght) {
			SetWallLenght (blockLenght);
			setLenght = false;
		}
		//block.lenght = blockLenght;
	}

	void SetVerticesMidle(WindowDoor windowDoor)
    {
        zedPosition = -3;
        switch (windowDoor.orientation) {
		case Enums.Orientation.vertical:
			{
				vertices [0] = new Vector3 (-Enums.depth, -windowDoor.lenght / 2, zedPosition);
				vertices [1] = new Vector3 (+Enums.depth, -windowDoor.lenght / 2, zedPosition);
				vertices [2] = new Vector3 (-Enums.depth, +windowDoor.lenght / 2, zedPosition);
				vertices [3] = new Vector3 (+Enums.depth, +windowDoor.lenght / 2, zedPosition);

				centerOfCollider = new Vector3 (0, 0, zedPosition);
				boxCollider.center = centerOfCollider;
				boxCollider.size = new Vector3 (Enums.depth * 2, windowDoor.lenght - colliderSizeOffset, .5f);
                    if (windowDoor.type == Enums.TypeOfIntegrable.window)
                    {
                        renderer.sharedMaterial = verMat;
                    }
			}
			break;

		case Enums.Orientation.horizontal:
			{
				vertices [0] = new Vector3 (- windowDoor.lenght / 2, - Enums.depth, zedPosition);
				vertices [2] = new Vector3 (- windowDoor.lenght / 2, + Enums.depth, zedPosition);
				vertices [1] = new Vector3 (+windowDoor.lenght / 2, -Enums.depth,  zedPosition);
				vertices [3] = new Vector3 (+windowDoor.lenght / 2, +Enums.depth, zedPosition);

				centerOfCollider = new Vector3 (0, 0, zedPosition);
				boxCollider.center = centerOfCollider;
				boxCollider.size = new Vector3 (windowDoor.lenght - colliderSizeOffset, Enums.depth * 2,  .5f);
                    if (windowDoor.type == Enums.TypeOfIntegrable.window)
                    {
                        renderer.sharedMaterial = horMat;
                    }
                }
			break;
		}

	}
	void SetVertices(WindowDoor block){
		switch (block.orientation2) {
		case Enums.Orientation2.top:
			{
				vertices [0] = new Vector3 ( - Enums.depth, 0, zedPosition);
				vertices [1] = new Vector3 ( + Enums.depth, 0, zedPosition);
				vertices [2] = new Vector3 ( - Enums.depth, +block.lenght, zedPosition);
				vertices [3] = new Vector3 ( + Enums.depth, +block.lenght, zedPosition);

				centerOfCollider = new Vector3 (0,+block.lenght/2, zedPosition/2);
				boxCollider.center = centerOfCollider;
				boxCollider.size = new Vector3 (Enums.depth*2,block.lenght-colliderSizeOffset, .5f);

			}
			break;
		case Enums.Orientation2.bottom:
			{
				vertices [0] = new Vector3 ( - Enums.depth, -block.lenght, zedPosition);
				vertices [1] = new Vector3 ( + Enums.depth, -block.lenght, zedPosition);
				vertices [2] = new Vector3 ( - Enums.depth, 0, zedPosition);
				vertices [3] = new Vector3 ( + Enums.depth, 0, zedPosition);

				centerOfCollider = new Vector3 (0, - block.lenght/2, zedPosition/2);
				boxCollider.center = centerOfCollider;
				boxCollider.size = new Vector3 (Enums.depth*2,block.lenght-colliderSizeOffset, .5f);
			
			}
			break;
		case Enums.Orientation2.right:
			{
				//lb
				vertices [0] = new Vector3 ( 0 ,           - Enums.depth, zedPosition);
				//rb
				vertices [1] = new Vector3 ( block.lenght, - Enums.depth, zedPosition);
				//lt
				vertices [2] = new Vector3 ( 0 ,             Enums.depth, zedPosition);
				//rt
				vertices [3] = new Vector3 ( block.lenght,   Enums.depth, zedPosition);

				centerOfCollider = new Vector3 (+block.lenght/2, 0, zedPosition/2);
				boxCollider.center = centerOfCollider;
				boxCollider.size = new Vector3 (block.lenght-colliderSizeOffset, Enums.depth*2, .5f);

			}
			break;
		case Enums.Orientation2.left:
			{
				vertices [1] = new Vector3 (  0,			 - Enums.depth, zedPosition);//rb
				vertices [0] = new Vector3 (- block.lenght,  - Enums.depth, zedPosition);//lb
				vertices [3] = new Vector3 (  0,               Enums.depth, zedPosition);//rt
				vertices [2] = new Vector3 (- block.lenght,    Enums.depth, zedPosition);//lt


				centerOfCollider = new Vector3 (-block.lenght/2, 0, 0);
				boxCollider.center = centerOfCollider;
				boxCollider.size = new Vector3 (block.lenght-colliderSizeOffset, Enums.depth*2, .5f);
			}
			break;
		}
		//if(vertices!=null)
		//base.shapeMesh.vertices = vertices; 
		
	}

	void SetMesh()
	{	
		int[] triangels = {0,2,1,3,1,2};
		Vector2[] uv = {new Vector2 (0,0),new Vector2 (0,1),new Vector2 (1,0),new Vector2 (1,1)};
		base.SetMesh (vertices, uv, triangels);
        
    }
	public void SetZedPosition()
	{
		transform.position = new Vector3 (transform.position.x, transform.position.y, zedPosition);
	}
	public void SetWindowDoor()
	{
		MaterialOfIntegrable (windowDoor.type);

		vertices = new Vector3[4];
		//block = new Block(Vector3.zero, 4f,ori) ;
		SetVerticesMidle (windowDoor);
		SetMesh ();
        try
        {
            MenuManager.instance.UpdateSelectedObjStats(objId.buildObj);
        }
        catch { }
    }
	public void SetWallLenght(float lenght)
	{
		blockLenght = lenght;
		windowDoor.lenght = lenght;
		SetWindowDoor();
	}
	public void CheckIfWePlaceItOnTheWall()
	{
	//	int layermask = 1>>8;
	//	RaycastHit hit;
		Ray landingRay = Camera.main.ScreenPointToRay (Input.mousePosition); 
		RaycastHit[] allHits;
		bool hittedWall=false;
		allHits=Physics.RaycastAll (landingRay, 100);
		//TODO OPTIMISE THIS PART LATER
		//if (Physics.RaycastAll (landingRay, out hit, 100,~layermask))
		foreach(var hit in allHits)
		{
			if (hit.collider.tag == "Wall") {
	
				windowDoor.orientation = hit.collider.GetComponent<VisualWall> ().wall.orientation;
				//ADD WINDOW TO WALL
				hit.collider.GetComponent<VisualWall> ().wall.windowsAndDoors.Add (windowDoor);
				//------------------
				windowDoor.inWall = true;
				windowDoor.parentWall = hit.collider.GetComponent<VisualWall> ().wall;

				SetWindowDoor ();
				BindToWall (hit);
				transform.SetParent (hit.collider.transform);
                if (windowDoor.orientation == Enums.Orientation.horizontal)
                {
                    transform.localPosition = new Vector3(transform.localPosition.x, 0 , transform.localPosition.z);
                }
                if (windowDoor.orientation == Enums.Orientation.vertical)
                {
                    transform.localPosition = new Vector3(0, transform.localPosition.y, transform.localPosition.z);
                }
                hittedWall = true;
				break;		
			} 
		}
		if (!hittedWall) {
			transform.position = GridBinder.BindToPos(transform.position);//GridBinder.BindToGrid (transform.position, GridBinder.gridPoints_little, GridBinder.l_Cell);
            SetZedPosition ();
			windowDoor.position = transform.position;		
			transform.SetParent (Manager.manager.transform.GetComponent<VisualHouse>().floors[Manager.manager.CurrentFlor].GetComponent<Transform>());
			windowDoor.inWall = false;
			windowDoor.parentWall = null;
		}
	}

	void BindToWall2(RaycastHit hit)
	{
		var visualWall = hit.collider.GetComponent<VisualWall>();

		if (visualWall.wall.orientation2 == Enums.Orientation2.bottom ||
			visualWall.wall.orientation2 == Enums.Orientation2.top) {
			transform.position = new Vector3 (visualWall.wall.position.x,hit.point.y);
			SetZedPosition ();
			windowDoor.position = transform.position;	
		} else
		{	
			transform.position = new Vector3 (hit.point.x,visualWall.wall.position.y);
			SetZedPosition ();
			windowDoor.position = transform.position;
		}		
	}
	void BindToWall(RaycastHit hit)
	{
		var visualWall = hit.collider.GetComponent<VisualWall>();

		if (visualWall.wall.orientation == Enums.Orientation.vertical) {
            transform.position = new Vector3 (visualWall.wall.position.x,hit.point.y-base.mouseOffset.y);
           // transform.localPosition = new Vector3(0, hit.point.y - base.mouseOffset.y);

            SetZedPosition ();
			windowDoor.position = new Vector3(0,transform.position.y);
		} else
		{
			transform.position = new Vector3 (hit.point.x-base.mouseOffset.x,visualWall.wall.position.y);
			SetZedPosition ();
			windowDoor.position = new Vector3(transform.position.x, 0);
        }
	}
	public 	void BindToWallGameObj(GameObject parentWall)
	{
		var visualWall = parentWall.GetComponent<VisualWall>();

		if (visualWall.wall.orientation == Enums.Orientation.vertical) {
			transform.position = new Vector3 (visualWall.wall.position.x,parentWall.transform.position.y);
			SetZedPosition ();
			windowDoor.position = transform.position;
		} else
		{

			transform.position = new Vector3 (parentWall.transform.position.x,visualWall.wall.position.y);
			SetZedPosition ();
			windowDoor.position = transform.position;
		}
	}

    public void SelectType()
    {
        typeOfObject = renderer.sharedMaterial == Manager.manager.GetComponent<Materials>().materials[1] ? Enums.TypeOfIntegrable.door : Enums.TypeOfIntegrable.window;
        objId.buildObj.objType = typeOfObject == Enums.TypeOfIntegrable.door ? ObjType.door : ObjType.window;
    }

	public void MaterialOfIntegrable(Enums.TypeOfIntegrable typeOfInteg)
	{
		switch (typeOfInteg) {
		case Enums.TypeOfIntegrable.door:
			{
				renderer.sharedMaterial = Manager.manager.GetComponent<Materials> ().materials [1];

				//Debug.Log("Door");
			}
			break;
		case Enums.TypeOfIntegrable.window:
			{
				//renderer.sharedMaterial = materials [1];
				renderer.sharedMaterial = Manager.manager.GetComponent<Materials> ().materials [2];
                   
                    //Debug.Log("Window");
                }
			break;
		}
	}

    protected new void OnCollisionEnter(Collision collisionInfo)
    {
        base.OnCollisionEnter(collisionInfo);
    }

    protected new void OnCollisionStay(Collision collisionInfo)
    {
        base.OnCollisionStay(collisionInfo);
    }
    protected new void OnCollisionExit(Collision collisionInfo)
    {
        base.OnCollisionExit(collisionInfo);
    }
}
