﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonClickPlaySound : MonoBehaviour {

    Button thisButton;

    void Start()
    {
        thisButton = GetComponent<Button>();

        thisButton.onClick.AddListener(() => { onButtonClick(); });
    }

    private void onButtonClick()
    {
        AudioHelper.Instance.PlayBtnSound();
    }
}
