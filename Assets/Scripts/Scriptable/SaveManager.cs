﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;

public class SaveManager : MonoBehaviour {

    private static SaveManager _instance;
    public static SaveManager Instance
    {   get{ return _instance;  }   }

    public List<PrefabTypes> prefabList;
    public List<BuildObj> existentIDsList;

    public List<BuildGameObj> buildGameObj;

    protected GameObject buildBase;
    protected VisualHouse visualHouse;

    public SavedProject savedProject;

    public bool exitSave = false;

    EventManager.EventWrapper saveObj;
    EventManager.EventWrapper clearAll;

#region UNITY CALLS
    void OnEnable()
    {
        saveObj = new EventManager.EventWrapper(delegate (MyEvent myEvent)
        {
            exitSave = true;
            StartCoroutine(TotalSaveToFile());             
        });
        EventManager.instance.Listen(EventManager.SAVE_OBJ, saveObj);

        clearAll = new EventManager.EventWrapper(delegate (MyEvent myEvent)
        {
            exitSave = false;
            StopAllCoroutines();
            SaveToFile(savedProject.path);
            SaveScreenShot();
            // Make ScreenShot                          ***************
            ClearBuildGameObjList();
            existentIDsList.Clear();
            Calculations.instance.walls.Clear();
            Calculations.instance.windowsAndDoors.Clear();
            Calculations.instance.fundaments.Clear();
            VisualHouse.instance.floors.Clear();
        });
        EventManager.instance.Listen(EventManager.CLEAR_ALL_STAFF, clearAll);
    }
    void OnDisable()
    {
        EventManager.instance.DestroyListener(EventManager.SAVE_OBJ, saveObj);
        EventManager.instance.DestroyListener(EventManager.CLEAR_ALL_STAFF, clearAll);
    }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
        DontDestroyOnLoad(this.gameObject);
    }

    void Start () {
        visualHouse = GetComponent<VisualHouse>();
        buildBase = GameObject.FindGameObjectWithTag("Finish");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
           // SaveToFile("file");
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
           // ReadFromFile("file");
        }
    }

    public void OnApplicationPause()
    {
        if (exitSave)
        {
            SaveToFile(savedProject.path);
            SaveScreenShot();
        }
    }

    public void OnApplicationQuit()
    {
        if (exitSave)
        {
            SaveToFile(savedProject.path);
            SaveScreenShot();
        }
    }

    #endregion
#region ObjInfo 
    public int SetObjID(BuildObj bObj )
    {
        if (existentIDsList.Count == 0)
        {

            int i = 0;  // first id 
            bObj.myID = i;
            existentIDsList.Add(bObj);
            return i;
        }
        else
        {
            BuildObj b = existentIDsList[existentIDsList.Count -1];
            int i = b.myID;
            i++;
            bObj.myID = i;
            existentIDsList.Add(bObj);
            return i;
        }
    }

    public void UpdateObjInfoInList(BuildObj buildObj)
    {
        for(int i=0;i< existentIDsList.Count;i++)
        {
            if (existentIDsList[i].myID == buildObj.myID)
            {
                existentIDsList[i] = buildObj;
            }
        }
    }

    public void RemoveObjects(BuildObj buildObj)
    {
        try
        {
            foreach (BuildGameObj b in buildGameObj)
            {
                if (buildObj == b.buildObj)
                {
                    buildGameObj.Remove(b);
                }
            }
        }
        catch { }
        existentIDsList.Remove(buildObj);
    }
#endregion
    #region SAVE READ
    public void SaveSettings(float soundVolume, float netVisibility)
    {
        PlayerPrefs.SetFloat("volume", soundVolume);
        PlayerPrefs.SetFloat("net", netVisibility);
    }

    public void LoadSettings(ref float volume, ref float net)
    {
        volume = PlayerPrefs.GetFloat("volume", 1);
        net = PlayerPrefs.GetFloat("net", 1);
    }

    protected IEnumerator TotalSaveToFile()
    {
        yield return new WaitForSeconds(0.5f);
        SaveToFile(savedProject.path);
        while (true)
        {
            yield return new WaitForSeconds(3f);
            SaveToFile(savedProject.path);
        }
    }

    protected void SaveScreenShot()
    {
        HiResScreenShots.Instance.TakeScreentShot(savedProject.name);
    }

    public void SaveToFile(string pathFile)
    {
        EventManager.instance.FireEvent(EventManager.SAVE_OBJ_LOCAL);
        File.WriteAllText(pathFile,"");

        string[] jsonStrings = new string[existentIDsList.Count];
        for (int i = 0; i < existentIDsList.Count; i++)
        {
            jsonStrings[i] = JsonUtility.ToJson(existentIDsList[i]);
        }

        File.WriteAllLines(pathFile, jsonStrings);
    }

    public void ReadFromFile(SavedProject sp)
    {
        savedProject = sp;
        ClearBuildGameObjList();

        string str = File.ReadAllText(savedProject.path);
        string[] result = str.Split(new string[] { "\n", "\r\n" }, System.StringSplitOptions.RemoveEmptyEntries);
        existentIDsList.Clear();
        
        foreach (string r in result)
        {
            BuildObj b = JsonUtility.FromJson<BuildObj>(r);
            existentIDsList.Add(b);
        }

        CreateObjects();

        visualHouse.UpdateFloursActivity();

    }
#endregion

    protected void CreateObjects()
    {
        for (int i = 0; i < existentIDsList.Count; i++)
        {
            for (int j = 0; j < prefabList.Count; j++)
            {
                if (existentIDsList[i].objType == prefabList[j].type)
                {
                    InstantiateObj(prefabList[j].prefab, existentIDsList[i]);
                }
            }
        }
    }

    protected void InstantiateObj(GameObject g , BuildObj b )
    {
        GameObject prefab =  Instantiate(g,b.pos,Quaternion.identity);

        buildGameObj.Add(new BuildGameObj(prefab , b));
        prefab.transform.SetParent(buildBase.transform);
        ObjectIdentity objectIdentity = prefab.GetComponent<ObjectIdentity>();
        objectIdentity.isLoadFromSave = true;
        objectIdentity.buildObj = b;

        if (b.objType == ObjType.flor)
        {
            print("floor add");
            visualHouse.floors.Add(prefab);
        }
        if (b.objType == ObjType.wall)
        {
            Calculations.instance.walls.Add(prefab);
        }
        if (b.objType == ObjType.door || b.objType == ObjType.window)
        {
            Calculations.instance.windowsAndDoors.Add(prefab);
        }
    }

    public GameObject SetUpObjHierarhy(BuildObj b )
    {
        if (b.parrentID < 0)
            return buildBase;

        for (int i = 0; i < existentIDsList.Count; i++)
        {
            if (buildGameObj[i].buildObj.myID == b.parrentID)
            {
                return buildGameObj[i].g;
            }
        }

        return buildBase;
    }

    protected void ClearBuildGameObjList()
    {
      //  foreach (BuildGameObj bgo in buildGameObj)
      //  {
          //  Calculations.instance.CheckDeletedObj(bgo.buildObj, gameObject);
          //  VisualHouse.instance.CheckDeletedObj(bgo.buildObj, gameObject);
      //      Destroy(bgo.g);
      //  }

        foreach (Transform child in buildBase.transform)
        {
            Destroy(child.gameObject);
        }

        buildGameObj.Clear();
    }
}
[System.Serializable]
public class BuildGameObj
{
    public GameObject g;
    public BuildObj buildObj;
    public BuildGameObj(GameObject g , BuildObj b )
    {
        this.g = g;
        this.buildObj = b;
    }
}

[System.Serializable]
public class PrefabTypes
{
    public ObjType type;
    public GameObject prefab;
}