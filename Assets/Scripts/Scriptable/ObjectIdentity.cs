﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectIdentity : MonoBehaviour {

    public BuildObj buildObj = new BuildObj(-2,-2, Vector3.zero,0,0,0, Enums.Orientation.horizontal);

    public bool isLoadFromSave;

    EventManager.EventWrapper deleteObj;
    EventManager.EventWrapper saveObj;

    void OnEnable()
    {
        deleteObj = new EventManager.EventWrapper(delegate (MyEvent myEvent)
        {
            int id = (int)myEvent.parameter;
            RemoveObj(id);
        });
        EventManager.instance.Listen(EventManager.DELETE_OBJ, deleteObj);

        saveObj = new EventManager.EventWrapper(delegate (MyEvent myEvent)
        {
            UpdateStats();
        });
        EventManager.instance.Listen(EventManager.SAVE_OBJ_LOCAL, saveObj);
    }
    void OnDisable()
    {
        EventManager.instance.DestroyListener(EventManager.DELETE_OBJ, deleteObj);
        EventManager.instance.DestroyListener(EventManager.SAVE_OBJ_LOCAL, saveObj);
    }

    void Start()
    {
        if (!isLoadFromSave)
        {
            InitIDs();
            print("InitIDs");
        }
        else
        {
            SetObjStats(buildObj);
            print("SetObjStats");
        }
    }

    void InitIDs()
    {
        UpdateParrent();
        InitType();
        buildObj.myID = SaveManager.Instance.SetObjID(buildObj);
        UpdatePos();
        UpdateStats();
    }

    void InitType()
    {
        if (GetComponent<VisualFloor>() != null)
        {
            buildObj.objType = ObjType.flor;
        }
        else
        {
            if (GetComponent<VisualFundament>() != null)
            {
                buildObj.objType = ObjType.fundament;
            }
            else
            {
                if (GetComponent<VisualWall>() != null)
                {
                    buildObj.objType = ObjType.wall;
                }
                else
                {
                    if (GetComponent<VisualWindowDoor>() != null)
                    {
                        print(GetComponent<VisualWindowDoor>().typeOfObject);
                        if (GetComponent<VisualWindowDoor>().typeOfObject == Enums.TypeOfIntegrable.door)
                            buildObj.objType = ObjType.door;
                        else
                            buildObj.objType = ObjType.window;
                    }
                }
            }
        }
    }

    public void UpdateParrent()
    {
        if (transform.parent != null)
        {
            if (transform.parent.GetComponent<ObjectIdentity>() != null)
            {
                // print(transform.name + " " + transform.parent.name + " " + transform.parent.GetComponent<ObjectIdentity>().buildObj.myID);
                buildObj.parrentID = transform.parent.GetComponent<ObjectIdentity>().buildObj.myID;
            }
            else
            {
                buildObj.parrentID = -1;
            }
        }  
    }

    public void UpdatePos()
    {
        buildObj.pos = transform.position;
        SaveManager.Instance.UpdateObjInfoInList(buildObj);
    }

    public void UpdateStats()
    {

        switch (buildObj.objType)
        {
            case ObjType.door: {
                    VisualWindowDoor visWindDoor = GetComponent<VisualWindowDoor>();
                    buildObj.ori = visWindDoor.windowDoor.orientation;
                    buildObj.width = 0;
                    buildObj.height = visWindDoor.windowDoor.heigh;
                    buildObj.length = visWindDoor.windowDoor.lenght;
                    break; }
            case ObjType.window: {
                    VisualWindowDoor visWindDoor = GetComponent<VisualWindowDoor>();
                    buildObj.ori = visWindDoor.windowDoor.orientation;
                    buildObj.width = 0;
                    buildObj.height = visWindDoor.windowDoor.heigh;
                    buildObj.length = visWindDoor.windowDoor.lenght;
                    break; }
            case ObjType.wall: {
                    VisualWall visWall = GetComponent<VisualWall>();
                    buildObj.ori = visWall.wall.orientation;
                    buildObj.width = 0;
                    buildObj.height = visWall.wall.heigh;
                    buildObj.length = visWall.wall.lenght;
                    break; }
            case ObjType.fundament: {
                    VisualFundament visFund = GetComponent<VisualFundament>();
                    buildObj.ori =  Enums.Orientation.horizontal;
                    buildObj.width = visFund.fundament.width;
                    buildObj.height = visFund.fundament.heigh;
                    buildObj.length = visFund.fundament.lenght;
                    break; }
            case ObjType.flor: { break; }
        }
        buildObj.pos = transform.position;
        try
        {
            SaveManager.Instance.UpdateObjInfoInList(buildObj);
        }catch{ }
    }



    protected void SetObjStats(BuildObj b)
    {
        GameObject parrent = SaveManager.Instance.SetUpObjHierarhy(b);
        print(b.parrentID+" "+ parrent.name);
        transform.SetParent(parrent.transform);
        transform.position = b.pos;
        switch (b.objType)
        {
            case ObjType.door:
                {
                    VisualWindowDoor visWindDoor = GetComponent<VisualWindowDoor>();
                    visWindDoor.windowDoor.orientation = b.ori;
                    visWindDoor.windowDoor.heigh = b.height;
                    visWindDoor.windowDoor.lenght = b.length;
                    SetWindDoorParentInfo();
                    break;
                }
            case ObjType.window:
                {
                    VisualWindowDoor visWindDoor = GetComponent<VisualWindowDoor>();
                    visWindDoor.windowDoor.orientation = b.ori;
                    visWindDoor.windowDoor.heigh = b.height;
                    visWindDoor.windowDoor.lenght = b.length;
                    SetWindDoorParentInfo();
                    break;
                }
            case ObjType.wall:
                {
                    VisualWall visWall = GetComponent<VisualWall>();
                    visWall.wall.orientation = b.ori;
                    visWall.wall.heigh = b.height;
                    visWall.wall.lenght = b.length;
                    //if (transform.parent.GetComponent<VisualFundament>() != null)
                    //{
                    //    visWall.wall.parentFundament = transform.parent.GetComponent<VisualFundament>().fundament;
                    //}
                    break;
                }
            case ObjType.fundament:
                {
                    VisualFundament visFund = GetComponent<VisualFundament>();
                    visFund.fundament.width = b.width;
                    visFund.fundament.heigh = b.height;
                    visFund.fundament.lenght = b.length;
                    break;
                }
            case ObjType.flor: { break; }
        }
    }

    private void SetWindDoorParentInfo()
    {
        if (transform.parent.gameObject != null)
        {
            if (transform.parent.GetComponent<VisualWall>() != null)
            {
                GetComponent<VisualWindowDoor>().windowDoor.parentWall = transform.parent.GetComponent<VisualWall>().wall;
            }
        }
    }

    public void RemoveObj(int id)
    {
       
        if (id == buildObj.myID || id == buildObj.parrentID)
        {
            SaveManager.Instance.RemoveObjects(buildObj);
            Calculations.instance.CheckDeletedObj(buildObj, gameObject);
        }
        else
        {
            if (transform.parent.GetComponent<ObjectIdentity>() != null)
            {
                if (id == transform.parent.GetComponent<ObjectIdentity>().buildObj.parrentID)
                {
                    SaveManager.Instance.RemoveObjects(buildObj);
                    Calculations.instance.CheckDeletedObj(buildObj, gameObject);
                  //  VisualHouse.instance.CheckDeletedObj(buildObj, gameObject);
                }
            }
        }
    }

}

[System.Serializable]
public enum ObjType { flor , fundament , wall , door , window}
[System.Serializable]
public class BuildObj
{
    public int parrentID;
    public int myID;
    public Vector3 pos;
    public ObjType objType;

    public float length;
    public float width;
    public float height;
    public Enums.Orientation ori;

    public BuildObj(int parrentID, int myID, Vector3 pos, float length, float width, float height, Enums.Orientation ori)
    {
        this.parrentID = parrentID;
        this.myID = myID;
        this.pos = pos;
        this.length = length;
        this.width = width;
        this.height = height;
        this.ori = ori;
    }
}
