﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHelper : MonoBehaviour {

    private static AudioHelper _instance;
    public static AudioHelper Instance
    {
        get { return _instance; }
    }

    protected AudioSource audioSource;
    public AudioClip btnClick;

    void Awake()
    {
        _instance = this;
    }

	void Start () {
        //  audioSource.clip = btnClick;
        audioSource = GetComponentInChildren<AudioSource>();
    }

    public void UpdateSoundActivity(float value)
    {
        audioSource.volume = value;
    }

    internal void PlayBtnSound()
    {
        audioSource.PlayOneShot(btnClick);
    }
}
