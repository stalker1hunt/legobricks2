﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class MenuManager : MonoBehaviour {

    public static MenuManager instance;

    public GameObject createProjWondow;
    public Text console;
    public GameObject projSavedContent;
    public GameObject projDeletedContent;
    public GameObject projSavedPrefab;
    public List<SavedProject> savedProjects;

    public GameObject errorName_Window;
    public GameObject confirmDelete;
    public InputField createFileName;
    public ScrollRect existedProjectsSlider;
    public ScrollRect deletedProjectsSlider;
    public Text selectedItemStats;
    public Text projNameText;
    public static string currentOpenedProj;

    protected SavedProject tempDeleteSP;

    void Awake()
    {
        if (instance == null)
            instance = this;
    }

    public void Start()
    {
        existedProjectsSlider.horizontalNormalizedPosition = 0;
        deletedProjectsSlider.horizontalNormalizedPosition = 0;
        confirmDelete.SetActive(false);

        GetSavedProjects();
        CreateProjButtons();
    }

    void DeleteProjImj()
    {
        string pathFile;
#if UNITY_EDITOR
        pathFile = Application.dataPath + "/StreamingAssets/";
#endif
#if UNITY_ANDROID && !UNITY_EDITOR
        pathFile = Application.persistentDataPath+"/";
#endif
        string[] result = tempDeleteSP.name.Split('.');
        string name = result[0];
        File.Delete(pathFile + "/" + name + ".png");
        print(pathFile + "/" + name + ".png");
    }

    void GetSavedProjects()
    {
        try
        {
            string pathFile;
#if UNITY_EDITOR       
            pathFile = Application.dataPath + "/StreamingAssets/";
#endif
#if UNITY_ANDROID && !UNITY_EDITOR
        pathFile = Application.persistentDataPath+"/";
#endif

            string[] files = Directory.GetFiles(pathFile, "*.json");
            console.text += files.Length;
            if (files.Length != 0)
            {
                foreach (string s in files)
                {
                    savedProjects.Add(new SavedProject(Path.GetFileName(s), s));
                }
            }

            console.text += pathFile;
        }
        catch {
            console.text += "GetSavedProjects error ";
        }
    }

    void CreateProjButtons()
    {
        if (savedProjects.Count != 0)
        {
            for (int i = 0; i < savedProjects.Count; i++)
            {
                SetUpProjButton(projSavedPrefab, projSavedContent, savedProjects[i]);
                SetUpProjButton(projSavedPrefab, projDeletedContent, savedProjects[i]);
            }
        }
    }
    protected void SetUpProjButton(GameObject prefab, GameObject parrent, SavedProject s)
    {
        GameObject b = Instantiate(prefab);
        b.transform.SetParent(parrent.transform);
        b.transform.localScale = new Vector3(1, 1, 1);
        ProjectButton pb = b.GetComponent<ProjectButton>();
        pb.savedProj = s;

        if (parrent == projSavedContent)
            pb.onClickProj = OnClickOpenProj;
        else
            pb.onClickProj = OnClickDeleteProj;

        pb.SetProjImage();
    }

    protected void SetProjName(string name)
    {
        string[] result = name.Split('.');
        currentOpenedProj = result[0];
        projNameText.text = currentOpenedProj;
    }

    public void UpdateSelectedObjStats(BuildObj buildObj)
    {
        string str = "";
        if (buildObj.objType == ObjType.fundament || buildObj.objType == ObjType.wall)
        {
            if (buildObj.length != 0)
                str += "Длина: " + buildObj.length + "м. ";
            if (buildObj.width != 0)
                str += "Ширина: " + buildObj.width + "м. ";
            if (buildObj.height != 0)
                str += "Высота: " + buildObj.height + "м.";
            
        }
        if (buildObj.objType == ObjType.window || buildObj.objType == ObjType.door)
        {
            if (buildObj.length != 0)
                str += "Ширина: " + buildObj.length + "м. ";
            if (buildObj.height != 0)
                str += "Высота: " + buildObj.height + "м.";
        }
        selectedItemStats.text = str;
    }
    #region OnClick
    public void OnClickChangeScreen(int state)
    {
        EventManager.instance.FireEvent(EventManager.SET_ACTIVE_PANEL, (MenuState)state);
    }

    public void OnClickCreateNewProj()
    {
        if (createFileName.text != "")
        {
            VisualHouse.instance.CreateFlour();
            Manager.manager.SetUpFloor();

            string name = createFileName.text;
            string path;
#if UNITY_EDITOR
            path = Application.dataPath + "/StreamingAssets/" + name + ".json";
#endif

#if UNITY_ANDROID && !UNITY_EDITOR
        path =  Application.persistentDataPath + "/"+ name+".json";
#endif
            SavedProject sp = new SavedProject(name, path);
            savedProjects.Add(sp);
            SaveManager.Instance.savedProject = sp;
            EventManager.instance.FireEvent(EventManager.SAVE_OBJ);

            SetUpProjButton(projSavedPrefab, projSavedContent, sp);
            SetUpProjButton(projSavedPrefab, projDeletedContent, sp);
            SetProjName(sp.name);
            OnClickChangeScreen((int)MenuState.Work);                                   // OPEN WORK SCREEN
        }
        else
        {
            createProjWondow.SetActive(false);
            errorName_Window.SetActive(true);
        }

    }

    public void OnClickOpenProj(SavedProject sp)
    {
        print("OPENED");
        SaveManager.Instance.ReadFromFile(sp);
        Manager.manager.SetUpFloor();
        EventManager.instance.FireEvent(EventManager.SAVE_OBJ);
        OnClickChangeScreen((int)MenuState.Work);                                     // OPEN WORK SCREEN                      
        SetProjName(sp.name);
    }

    public void OnClickDeleteProj(SavedProject sp)
    {
        print("DELETE");
        confirmDelete.SetActive(true);
        tempDeleteSP = sp;
    }

    public void OnClick_OK_Delete()
    {
        if (tempDeleteSP != null)
        {
            EventManager.instance.FireEvent(EventManager.DELETE_MENU_OBJ, tempDeleteSP);
            File.Delete(tempDeleteSP.path);
            DeleteProjImj();
            savedProjects.Remove(tempDeleteSP);
            tempDeleteSP = null;
        }
    }

    public void OnClickBackToMenu() // clear all build staff 
    {
        EventManager.instance.FireEvent(EventManager.CLEAR_ALL_STAFF);
    }
#endregion
    
}

[System.Serializable]
public class SavedProject
{
    public string name;
    public string path;

    public SavedProject(string name, string path)
    {
        this.name = name;
        this.path = path;
    }
}