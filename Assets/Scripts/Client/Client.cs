﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class MyMsgType
{
    public static short formSender = 48;
}

public class FormSender : MessageBase
{
    public string Name;

    public override void Serialize(NetworkWriter writer)
    {
        writer.Write(Name);
    }
}

public class Client : MonoBehaviour
{

    NetworkClient myClient;

    [SerializeField]
    private string Ip;

    private void Awake()
    {
        DontDestroyOnLoad(this);
        SetupClient();
    }

    private void Start()
    {
      //  MailSender();
    }

    public void SetupClient()
    {
        myClient = new NetworkClient();
        myClient.RegisterHandler(MsgType.Connect, OnConnected);
        myClient.RegisterHandler(MyMsgType.formSender, FormSenderHandler);
        myClient.Connect(Ip, 4444);

    }

    public void OnConnected(NetworkMessage netMsg)
    {
        Debug.Log("Connected to server");
    }

    public void FormSenderHandler(NetworkMessage netMsg)
    {
        Debug.Log("testHandleForm");
    }

    public void MailSender(string name)
    {
        FormSender msg = new FormSender();
        msg.Name = name;

      //.Send(MyMsgType.formSender, msg);
    }
}
