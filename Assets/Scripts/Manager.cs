﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Manager : MonoBehaviour {
	public int gridHeigh;
	public int gridWidth;
	public Enums.Orientation blockOrientation;
	public Enums.Tool currentTool;

    public Text allFloorLB;
    [HideInInspector]
	public VisualHouse house;
	[HideInInspector]
	public GameObject activeFloor;
	int currentFlor=0;

    public static float currentMapLine = 1.5f;
    public static float baseMapLine = 1.5f;

    public int CurrentFlor {
		get {
			return currentFlor;
		}
		set {
            //house.SetActiveFloor (value);
            int floor = value + 1;
			currentFlor = value;
			activeFloor = VisualHouse.instance.floors [CurrentFlor];
            UpdateFloorName();

        }
	}

	//----------UI windows zone------------
	public GameObject wallParamWindow;
	public GameObject windowDoorParamWindow;
	public GameObject fundamentParamWindow;
	public GameObject ParamWindow;
	public GameObject addObjectsWindow;
	public GameObject calculationWindow;
	public GameObject deleteProjectWindow;
	public GameObject createProjectWindow;

	//Camera mainCamera;
	GameObject tempPrefab;
	MenuManager menuManager;
	
	public GameObject selectedObject;
    public GameObject wallPrefab;
	public GameObject integrablePrefab;
	public GameObject fundamentPrefab;
	public GameObject floorPrefab;

	// Use this for initialization
	public static Manager manager;

    [Header("Camera")]
    public Camera _camera;
    List<VisualWall> wallsList;
	List<VisualWindowDoor> WindowDoorList;
	Vector3 offsetVector;

    public float timeCoolDown = 0.5f;
    public float timer = 0;
    public static bool IsShow = false;

    void Awake(){

		offsetVector = new Vector3 (10, 10, 0);
		if(manager==null)
		manager = this;
	}
	void Start()
	{

        menuManager = GameObject.Find("MenuManager").GetComponent<MenuManager>();
	//	SetCameraPosition (houseBase.fundament.width+houseBase.fundament.position.x, houseBase.fundament.heigh+houseBase.fundament.position.y);
	//	houseBase.SetMesh ();
	}
	
	// Update is called once per frame

	void Update()
	{
		

        CheckSelectedObject();
        IsLighting();
    }

    public void IsLighting()
    {
        if (timer > timeCoolDown)
        {
            // UpdateMeshEnable();
            IsShow = !IsShow;
            timer = 0;
        }
        else
        {
            timer += Time.deltaTime;
        }
    }


    public void UpdateFloorName()
    {
        allFloorLB.text = "Этаж " + (currentFlor + 1) + "/" + VisualHouse.instance.floors.Count;
    }

   

    public void SetUpFloor()
    {
        CurrentFlor = 0;
        if (transform.GetComponent<VisualHouse>().floors.Count > 0)
            activeFloor = VisualHouse.instance.floors[CurrentFlor];
    }

    private void CheckSelectedObject()
    {
        bool value = selectedObject != null ? true : false;
        ParamWindow.SetActive(value);
    }

	bool IsSlotFree(Vector3 point,Enums.Orientation2 ori,float length)
		{
			RaycastHit hit;
			if (Physics.Raycast (point, orientationToVector3Direction (ori), out hit, length)) {
				//Debug.DrawLine (point, hit.point, Color.green);
				//Debug.Log ("Slot is not empty");

				return false;
			} else 
			{
				//Debug.Log ("No hit!");
				return true;
			}
		}
	Vector3 orientationToVector3Direction(Enums.Orientation2 ori)
	{
		switch (ori) {
		case (Enums.Orientation2.bottom):
			return Vector3.down; 
			break;
		case (Enums.Orientation2.top):
			return Vector3.up; 
			break;
		case (Enums.Orientation2.left):
			return Vector3.left; 
			break;
		case (Enums.Orientation2.right):
			return Vector3.right; 
			break;
		}
		Debug.LogError ("Wrong Something wrong with Enums.orientation enum!!!!");
		return Vector3.zero;
	}
	void SetCameraPosition(float x, float y)
	{
//		Camera.main.transform.position = new Vector3 
//		(
//		houseBase.fundament.width/2, houseBase.fundament.heigh/2, -(Mathf.Max (x, y) + 4f)
//		);
//		Camera.main.orthographicSize = (Mathf.Max (x, y) );
	}

	public void OnSelectObject(GameObject selectedObject){

		if (selectedObject.GetComponent<VisualWall> () != null) {
			
		
			WallParamWindow wind = wallParamWindow.GetComponent<WallParamWindow> ();
			wind.selectedObject = selectedObject;
			wind.lenght.placeholder.GetComponent<Text> ().text = selectedObject.GetComponent<VisualWall> ().wall.lenght.ToString ();
			wind.heigh.placeholder.GetComponent<Text> ().text = selectedObject.GetComponent<VisualWall> ().wall.heigh.ToString ();
		
			wind.orientation.value = (int)selectedObject.GetComponent<VisualWall> ().wall.orientation;
			//wind.title.text = selectedObject.name;
			wallParamWindow.SetActive (true);
		} 
		if(selectedObject.GetComponent<VisualWindowDoor> () != null)
		{


			WindowDoorParamWindow wind = windowDoorParamWindow.GetComponent<WindowDoorParamWindow> ();
			wind.selectedObject = selectedObject;
			wind.lenght.placeholder.GetComponent<Text> ().text = selectedObject.GetComponent<VisualWindowDoor> ().windowDoor.lenght.ToString ();
			wind.heigh.placeholder.GetComponent<Text> ().text = selectedObject.GetComponent<VisualWindowDoor> ().windowDoor.heigh.ToString ();
			//wind.title.text = selectedObject.name;
			windowDoorParamWindow.SetActive (true);
		}

		if (selectedObject.GetComponent<VisualFundament> () != null) {
			FundamentParamWindow  wind = fundamentParamWindow.GetComponent<FundamentParamWindow> ();

			wind.selectedObject = selectedObject;
			wind.lenght.placeholder.GetComponent<Text> ().text = selectedObject.GetComponent<VisualFundament> ().fundament.lenght.ToString ();
			wind.heigh.placeholder.GetComponent<Text> ().text = selectedObject.GetComponent<VisualFundament> ().fundament.heigh.ToString ();
			wind.width.placeholder.GetComponent<Text> ().text = selectedObject.GetComponent<VisualFundament> ().fundament.width.ToString ();
			//wind.title.text = selectedObject.name;
			fundamentParamWindow.SetActive (true);
		}
	}


    public void OnClickSave()
    {
     //   EventManager.instance.FireEvent(EventManager.SAVE_OBJ);
    }

	public void OnClickAddDoorButton(){
		GameObject integrableInst = Instantiate (integrablePrefab);

		//CHECK IS IT CORRECT
		integrableInst.transform.SetParent (VisualHouse.instance.floors[CurrentFlor].transform);
		//------------------

		integrableInst.transform.position =offsetVector;			
		tempPrefab = integrableInst;
		tempPrefab.GetComponent<VisualWindowDoor> ().windowDoor = new WindowDoor (offsetVector, Enums.defaultDoorLenght, Enums.defaultDoorlHeight, Enums.TypeOfIntegrable.door,Enums.Orientation.horizontal);
		tempPrefab.GetComponent<VisualWindowDoor> ().SetWindowDoor();
        //Calculations.instance.windowsAndDoors.Add(integrableInst);
		//addObjectsWindow.SetActive (false);
	}
	public void OnClickAddWindowButton()
	{
		GameObject integrableInst = Instantiate (integrablePrefab);

		//CHECK IS IT CORRECT
		integrableInst.transform.SetParent (VisualHouse.instance.floors[CurrentFlor].transform);
        //------------------
        
        integrableInst.transform.position = offsetVector;			
		tempPrefab = integrableInst;
		tempPrefab.GetComponent<VisualWindowDoor> ().windowDoor = new WindowDoor (offsetVector,Enums.defaultWindowLenght, Enums.defaultWindowlHeight, Enums.TypeOfIntegrable.window, Enums.Orientation.horizontal);
		tempPrefab.GetComponent<VisualWindowDoor> ().SetWindowDoor();

       // Calculations.instance.windowsAndDoors.Add(integrableInst);
    }

	public void OnClickAddWallButton(){
		GameObject wallInst = Instantiate (wallPrefab);

		//CHECK IS IT CORRECT
		wallInst.transform.SetParent (VisualHouse.instance.floors[CurrentFlor].transform);
		//------------------

		wallInst.transform.position =offsetVector;			
		tempPrefab = wallInst;
		tempPrefab.GetComponent<VisualWall> ().wall = new Wall (offsetVector, Enums.defaultWallLenght,Enums.defaultWallHeight, Enums.Orientation.horizontal);
		tempPrefab.GetComponent<VisualWall> ().UpdateWall ();

        Calculations.instance.walls.Add(wallInst);

	}
	public void OnClickAddFundament()
	{
		GameObject fundamentInst = Instantiate (fundamentPrefab);

		//CHECK IS IT CORRECT
		fundamentInst.transform.SetParent (transform.GetComponent<VisualHouse>().floors[CurrentFlor].GetComponent<Transform>());
		//------------------

		fundamentInst.transform.position = offsetVector;			
		tempPrefab = fundamentInst;
		tempPrefab.GetComponent<VisualFundament> ().fundament = new Fundament (Enums.defaultFundamentLenght, Enums.defaultFundamentWidth, Enums.defaultFundamentHeight, offsetVector);
		tempPrefab.GetComponent<VisualFundament> ().SetVisualFundament();
	}
	public void OnClickDeleteSelectedObject(){
		//if(selectedObject.GetComponent<VisualWall>()!=null)
  //          Calculations.instance.walls.Remove(selectedObject);
		//if(selectedObject.GetComponent<VisualWindowDoor>()!=null)
  //          Calculations.instance.windowsAndDoors.Remove(selectedObject);

        EventManager.instance.FireEvent(EventManager.DELETE_OBJ, selectedObject.GetComponent<ObjectIdentity>().buildObj.myID);
		Destroy (selectedObject.gameObject);

    }
	public void OnClickShowObjectParam(){
		OnSelectObject(selectedObject);
	}
	public void OnCalculationIcoClick(){
        Calculations.instance.Calculate ();
		//calculationWindow.SetActive (true);
		calculationWindow.GetComponent<CalculationWindow> ().price.text = Calculations.instance.price.ToString("F2");
		calculationWindow.GetComponent<CalculationWindow> ().volumeInBricksLabel.text = Calculations.instance.volumeInBricks.ToString("F2");
		calculationWindow.GetComponent<CalculationWindow> ().volumeInCubicMetresLabel.text = Calculations.instance.volumeInCubicMetres.ToString("F2");
	}
	public void OnClickAddObjects()	{
		addObjectsWindow.SetActive(!addObjectsWindow.activeSelf);
	}

	public void OnClickDeleteProject(){
		deleteProjectWindow.SetActive (true);
	}

	public void OnClickCreateNewProject(){
		createProjectWindow.SetActive (true);
	}

	public void OnCancelClick(GameObject window){
		window.SetActive (false);
	}

	public void HideAllParametsWindows(){
		wallParamWindow.SetActive(false);
		windowDoorParamWindow.SetActive(false);
		fundamentParamWindow.SetActive(false);
		addObjectsWindow.SetActive(false);
	}
	public void OnAddFloorClick()
	{
        VisualHouse.instance.OnAddFloorClick();
        UpdateFloorName();

    }
	public void OnNextFloorClick()
	{
		CurrentFlor = GetComponent<VisualHouse> ().ActiveNextFloor (CurrentFlor);
	}
	public void OnPreviouseFloorClick()
	{
		CurrentFlor = GetComponent<VisualHouse> ().ActivePreviousFloor(CurrentFlor);
	}
	/*
#region Not in use
	public void OnSelectToolClick(Button bt)
	{
		if (currentTool != Enums.Tool.select) {
			currentTool = Enums.Tool.select;
			bt.image.color = Color.green;
		} else {
			bt.image.color = Color.white;
			currentTool = Enums.Tool.spawn;
		}

	}
	public void SpawnChosenObject(RaycastHit hit, Enums.BlockType bt)
	{
		switch(bt)
		{
		case Enums.BlockType.fundamental:
			{
				if (hit.collider.tag == "House Base") {	
					//Debug.Log (hit.collider.name);
					Vector3 temp = GridBinder.BindToGrid (hit.point);
					if (IsSlotFree ((Vector3)temp, blockOrientation, 1.9f)) {
						GameObject wallInst = Instantiate (wallPrefab);
						wallInst.transform.position = new Vector3 (temp.x, temp.y, -1f);			
						tempPrefab = wallInst;
						tempPrefab.GetComponent<VisualWall> ().wall = new Wall (temp, Enums.defaultWallLenght,Enums.defaultWallHeight, blockOrientation);
						tempPrefab.GetComponent<VisualWall> ().UpdateWall ();
						tempPrefab.GetComponent<VisualWall> ().wall.position = tempPrefab.transform.position;
					}
				}
			}
			break;
		case Enums.BlockType.integrable:
			{
				if (hit.collider.tag == "Wall") {	
					//Debug.Log (hit.collider.name);
					Vector2 temp = GridBinder.BindToGrid (hit.point);
					if (IsSlotFree ((Vector3)temp, blockOrientation, 1.9f)) {


						GameObject integrableInst = Instantiate (integrablePrefab);
						integrableInst.transform.parent = hit.collider.gameObject.transform; 
						integrableInst.GetComponent<VisualInegrable> ().integrable.parentWall = hit.collider.gameObject.GetComponent<VisualWall>().wall;
						hit.collider.gameObject.GetComponent<VisualWall> ().integrableList.Add (integrableInst);
						integrableInst.transform.position = new Vector3 (hit.point.x,hit.point.y, -2f);	
						integrableInst.GetComponent<VisualInegrable> ().SetParametrs();
						//	Debug.Log (integrablePrefab.transform.position);
						tempPrefab =  integrablePrefab;
						//	tempPrefab.GetComponent<VisualInegrable>().SetParametrs
						//tempPrefab.GetComponent<VisualIntegrable> ().wall = new Wall (temp, blockLenght, blockOrientation);
						if (tempPrefab.GetComponent<VisualInegrable>().integrable!=null)
							tempPrefab.GetComponent<VisualInegrable> ().UpdateIntegrable();
						//	tempPrefab.GetComponent<VisualInegrable> ().ApplyEditorChanges = true;
					}
				}

			}
			break;

		case Enums.BlockType.fundament:
			{
				if (hit.collider.tag == "Background") {	

					Debug.Log (hit.collider.name);
					Vector2 temp;
					if (hit.point.x > 0 || hit.point.y > 0) {	
						temp = GridBinder.BindToGrid (hit.point);
						GameObject fundamentInst = Instantiate (fundamentPrefab);
						fundamentInst.transform.position = new Vector3 (temp.x, temp.y, 0f);			
						tempPrefab = fundamentInst;
						tempPrefab.GetComponent<VisualFundament> ().fundament = new Fundament (5f,5f,2f,temp);
						tempPrefab.GetComponent<VisualFundament> ().SetVisualFundament();
						//						tempPrefab.GetComponent<VisualWall> ().wall = new Wall (temp, blockLenght, blockOrientation);
						//						tempPrefab.GetComponent<VisualWall> ().UpdateWall ();
						//						tempPrefab.GetComponent<VisualWall> ().wall.position = tempPrefab.transform.position;
					}
				}	
			}
			break;
		default:
			break;
		}
	}
	GameObject SpawnPrefab(Enums.BlockType bt)
	{
		switch (bt) {
		case Enums.BlockType.fundamental:
			{
				return wallPrefab;
			}
		case Enums.BlockType.integrable:
			{
				return integrablePrefab;
			}
		default: 
			return null;
			break;
		}
		Debug.LogError ("Wrong input data for SpawnPrefab function (Manager)!");
		return null;

	}
#endregion
	*/
}
