﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotTexture : MonoBehaviour {

    public int angle = 30;
    public int speed = 2;
    Renderer renderer;
    // Use this for initialization
    void Start()
    {
        renderer = transform.GetComponent<Renderer>();
        Quaternion rot = Quaternion.Euler(0, 90, 0);
        Matrix4x4 m = new Matrix4x4();
        m.SetTRS(Vector3.zero, rot, new Vector3(1, 1, 1));
        renderer.material.SetMatrix("_Rotation", m);
    }

    // Update is called once per frame
    void Update()
    {
        //for fixed rotation uncomment the line below:
        //Quaternion rot = Quaternion.Euler (0, angle, 0);

        //for animated cubemap:

    }

}
