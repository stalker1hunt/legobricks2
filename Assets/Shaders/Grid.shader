﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Unlit/Grid"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}

		_LineWidth("Line width",Float) = 1
		_Step ("Step",Float) = 1
		_Width ("Width",Float) = 1
		_Heigh ("Heigh",Float) = 1

	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog

			uniform float _Step;
		    uniform float _Width;
		    uniform float _Heigh;
		    uniform float _LineWidth;
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
		//		float2 uv : TEXCOORD0;

			};

			struct v2f
			{
				//float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 vertex : SV_POSITION;
				float4 pos_in_world_space: TEXCOORD0;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			
			v2f vert (appdata v)
			{
				v2f o;
			//	o.vertex = UnityObjectToClipPos(v.vertex);
			//	o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				o.vertex = UnityObjectToClipPos(v.vertex);
				o.pos_in_world_space = mul(unity_ObjectToWorld,v.vertex);

				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}

			float MaxNumber (float A, float B)
			{
			if (A>B) return A; 
			else return B;
			}

			fixed4 frag (v2f IN) : SV_Target
			{
				// sample the texture
				fixed4 col = fixed4(.8,.8,.8,0.0);// tex2D(_MainTex, IN.uv);

				float maxNumber= MaxNumber(_Width,_Heigh);
				float lv= _LineWidth/100;



//				for(int i=0;i<(maxNumber)*_Step;i+=_Step)
//				{
//
//					if(i<(_Width)*_Step)
//					{
//						if((IN.uv.x>=(i-lv)/(maxNumber*_Step))&&(IN.uv.x<=(i+lv)/(maxNumber*_Step)))
//						col=fixed4(1.0,.0,.0,1.0);
//					}
//					if(i<(_Heigh)*_Step)
//					{
//						if((IN.uv.y>=(i-lv)/(maxNumber*_Step))&&(IN.uv.y<=(i+lv)/(maxNumber*_Step)))
//						col=fixed4(1.0,.0,.0,1.0);
//					} 
//				}

				for(int i=0;i<(maxNumber)*_Step;i+=_Step)
				{

					if(i<(_Width)*_Step)
					{
						if((IN.pos_in_world_space.x>=(i-lv))&&(IN.pos_in_world_space.x<=(i+lv)))
						col=fixed4(.0,.0,.0,.0);
					}
					if(i<(_Heigh)*_Step)
					{
						if((IN.pos_in_world_space.y>=(i-lv))&&(IN.pos_in_world_space.y<=(i+lv)))
						col=fixed4(.0,.0,.0,.0);
					} 
				}
			

				// apply fog
				UNITY_APPLY_FOG(IN.fogCoord, col);

				return col;
			}
			ENDCG
		}
	}
}
